# Comunidad

# Se importan los modulos necesarios
import random as r
import time as t
from person import Person

from vaccine import Vaccine

from illness import Illness
from condition import Condition

r.seed(t.time())

class Community():
    """ Clase dedicada al objeto tipo Community """
    def __init__(self, people, average_physical_connection, disease,
                physical_connection_probability):
        # Se establece la comunidad
        self.num_people = people
        self.total_vax = self.num_people//2
        self.total_vax1 = self.total_vax//2
        self.total_vax2 = self.total_vax//3
        self.total_vax3 = self.total_vax//6
        self.pro_vax1 = []
        self.pro_vax2 = []
        self.pro_vax3 = []
        self.average_physical_connection = average_physical_connection
        self.disease = disease
        self.physical_connection_probability =  physical_connection_probability
        # Lista donde se conservaran los objetos Person
        self.hab_list = []
        self.conditions = [Condition.obesity(), Condition.malnutrition()]
        self.illnesses = [Illness.asthma(), Illness.cereb_disease(),
                        Illness.cystic_fibrosis(), Illness.hypertension()]


        # Metodos utilizados para inicializar elementos de la comunidad
        self.create_people()
        self.create_contacts()
        self.create_family()
        self.apply_base_condition()
        self.apply_base_illness()
        self.choose_vaccinated()

    def create_people(self):
        """ Se crean personas """
        for x in range(self.num_people):
            average = self.average_physical_connection
            contact_length = r.randint(average-2, average+2)
            family_length = r.randint(0, 5)
            human_being = Person(self, id, self.disease)
            human_being.len_contacts = contact_length
            human_being.len_family = family_length
            human_being.vaccine = Vaccine.no_vaccine()
            human_being.base_condition = Condition.no_condition()
            human_being.base_illness = Illness.no_illness()
            human_being.age = r.randint(1, 90)
            if human_being.age <= 30:
                human_being.age_risk = 0.15
            elif 30 < human_being.age <= 60:
                human_being.age_risk = 0.3
            else:
                human_being.age_risk = 0.5
            human_being.set_severity()
            self.hab_list.append(human_being)


    def apply_base_condition(self):
        """ Se aplican las condiciones de base al 65% de la poblacion """
        num_afected = int(self.num_people * 0.65)
        afected = r.sample(self.hab_list, num_afected)
        for person in afected:
            person.base_condition = r.choice(self.conditions)
            person.set_severity()


    def apply_base_illness(self):
        """ Se aplican las enfermedades de base al 25% de la poblacion """
        num_afected = int(self.num_people * 0.25)
        afected = r.sample(self.hab_list, num_afected)
        for person in afected:
            person.base_illness = r.choice(self.illnesses)
            person.set_severity()


    def choose_vaccinated(self):
        """Se seleccionan los vacunados y el tipo de vacuna de cada uno"""
        # Lista de todos los que seran vacunados
        vaccinated = r.sample(self.hab_list, self.total_vax)
        for person in vaccinated:
            person.pro_vaccine = True

        # La lista de beneficiarios se divide en 3 listas segun el tipo de vacuna
        self.pro_vax1 = vaccinated[:self.total_vax1]
        self.pro_vax2 = vaccinated[self.total_vax1-1:self.total_vax1+self.total_vax2-1]
        self.pro_vax3 = vaccinated[self.total_vax2+self.total_vax1+1:]

        # La lista de cada tipo de vacuna se guarda en una matriz
        self.vax_groups = [self.pro_vax1, self.pro_vax2, self.pro_vax3]


    def random_person(self):
        """ Se selecciona una persona aleatoria de la lista de habitantes """
        return r.choice(self.hab_list)


    def create_contacts(self):
        """ Se crean los contactos de cada persona """
        for person in self.hab_list:
            contact_length = person.len_contacts - len(person.contacts)
            if contact_length <= 0:
                continue

            # Chequea que la persona no este dentro de sus contactos
            sample = r.sample(self.hab_list, contact_length)
            while person in sample:
                sample = r.sample(self.hab_list, contact_length)

            # Se forma el enlace persona-contacto
            for contact in sample:
                if not len(contact.contacts) == contact.len_contacts:
                    if not person in contact.contacts:
                        person.contacts.append(contact)
                        contact.contacts.append(person)


    def create_family(self):
        """ Se crea la familia de cada persona """
        for person in self.hab_list:
            family_length = person.len_family - len(person.family)
            if family_length <= 0:
                continue

            # Chequea que la persona no este dentro de su propia familia
            sample = r.sample(self.hab_list, family_length)
            while person in sample:
                sample = r.sample(self.hab_list, family_length)

            # Chequea que la familia no se repita con los contactos
            while True:
                flag = 0
                for relative in sample:
                    if relative in person.contacts:
                        flag = 1
                        sample.remove(relative)
                        new_relative = self.random_person()
                        while new_relative is person:
                            new_relative = self.random_person()
                        sample.append(new_relative)
                if flag == 0:
                    break

            # Se forma el enlace persona-familiar
            for relative in sample:
                if not len(relative.family) == relative.len_family:
                    if not person in relative.family:
                        person.family.append(relative)
                        relative.family.append(person)


    def init_report(self):
        """ Reporte inicial de la simulacion, con los datos que se piden """
        total = len(self.hab_list)
        pw_base_problem = 0
        sum_age = 0
        available_vaxs = 2*len(self.pro_vax1) + 2*len(self.pro_vax2) + len(self.pro_vax3)
        for person in self.hab_list:
            sum_age += person.age
            if person.base_condition.name != "Sin condicion" or person.base_illness.name != "Sin enfermedad":
                pw_base_problem += 1
        age_average = round(sum_age/total)
        print("="*80)
        print("Reporte Inicial:")
        print(f"Total poblacion: {total}")
        print(f"Personas con enfermedad o condicion: {pw_base_problem}")
        print(f"Promedio edades: {age_average}")
        print(f"Vacunas disponibles: {available_vaxs}")


    def final_report(self):
        """ Reporte final de la simulacion, con los datos que se piden """
        total = len(self.hab_list)
        pw_base_problem = 0
        sum_age = 0
        total_ill = 0
        vaccinated = 0
        immunes = 0
        deaths = 0
        for person in self.hab_list:
            if not person.is_dead:
                sum_age += person.age
            if person.base_condition.name != "Sin condicion" or person.base_illness.name != "Sin enfermedad":
                pw_base_problem += 1
            if not person.healthy:
                total_ill += 1
            if person.vaccine.vax_id != 0:
                vaccinated += 1
            if person.done and not person.is_dead:
                immunes += 1
            if person.is_dead:
                deaths += 1
        age_average = round(sum_age/total)

        print("="*80)
        print("Reporte Final:")
        print(f"Total poblacion: {total}")
        print(f"Personas con enfermedad o condicion: {pw_base_problem}")
        print(f"Total contagiados: {total_ill}")
        print(f"Total inoculados: {vaccinated}")
        print(f"Total de inmunes o recuperados: {immunes}")
        print(f"Total fallecidos: {deaths}")
        print(f"Promedio edades de los vivos: {age_average}")


    def report(self):
        """ Reporte que se hace por cada paso de la simulacion """
        healthy = 0
        done = 0
        ill = 0
        deaths = 0
        severes = 0
        for person in self.hab_list:
            if not person.done and not person.healthy:
                ill += 1
            elif not person.done and person.healthy:
                healthy += 1
            elif person.done and not person.is_dead:
                done += 1
            elif person.is_dead:
                deaths += 1
            if person.is_severe and not person.done:
                severes += 1

        print(f"{healthy} susceptibles, {done} recuperados, {ill} infectados, {severes} graves, {deaths} muertos")
