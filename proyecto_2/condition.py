# Condition

class Condition():
    """ Clase dedicada a la condicion de base """
    def __init__(self, name, gravity):
        # Se establece la condicion de base
        self.name = name
        self.gravity = gravity

    @classmethod
    # Obesidad
    def obesity(cls):
        return cls("Obesidad", 0.5)

    @classmethod
    # Desnutricion
    def malnutrition(cls):
        return cls("Desnutricion", 0.2)

    @classmethod
    # Sin condicion
    def no_condition(cls):
        return cls("Sin condicion", 0)
