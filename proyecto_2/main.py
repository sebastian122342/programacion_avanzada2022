# Main

"""
Proyecto 2 programacion avanzada

Se hace una simulacion de la propagacion de una enfermedad, en una poblacion
determinada.

Esta simulacion se hace apegandose al modelo SIR.
Sin embargo hay algunas modificaciones, hay vacunas, conteo de muertos,
enfermedades y condiciones de base y edades, que permiten que no todas las
personas sean igual de susceptibles ante la enfermedad.
"""

# Se importan los modulos necesarios
from disease import Disease
from person import Person
from community import Community
from simulator import Simulator

# Se crea enfermedad, comunidad y se ejecuta la simulacion
covid = Disease(0.6, 14)
talca = Community(1000, 5, covid, 0.7)
sim = Simulator(talca)
sim.run(150)
