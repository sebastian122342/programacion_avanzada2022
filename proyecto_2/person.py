# Persona

# Se importa el modulo necesario
import random as r

class Person():
    """ Clase dedicada al objeto tipo Person """
    def __init__(self, community, id, disease):
        # Se establece a la persona y sus atributos
        self.vaccine = None
        self.pro_vaccine = False
        self.current_step_vaccine = None
        self.doses = 0
        self.age = None
        self.age_risk = None
        self.severity = None
        self.is_severe = None
        self.base_condition = None
        self.base_illness = None
        self.community = community
        self._id = id
        self.len_family =  None
        self.family = []
        self.disease = disease
        self.done = False
        self.is_dead = False
        self.healthy = True
        self.total_step_disease = None
        self.current_step_disease = None
        self.len_contacts = None
        self.contacts = []

    # Getter y Setter para el id de la persona
    @property
    def id(self):
        return self._id
    @id.setter
    def id(self, id):
        self._id = id


    def set_infected(self):
        """ Metodo creado para enfermar a una persona """
        self.healthy = False
        # Se determina cuanto durara la enfermedad
        self.total_step_disease = self.disease.average_steps + r.randint(-2, 2)
        self.current_step_disease = 0


    def set_recovered(self):
        """ Se establece que la persona esta lista: muerta o recuperada """
        self.done = True


    def dies(self):
        """ Muerte de una persona """
        self.is_dead = True
        self.done = True


    def encounter(self, group):
        """ Encuentro fisico de una persona con otra, familiar o contacto """
        if len(group) == 0:
            return None
        prob = self.community.physical_connection_probability
        if prob * 100 >= r.randint(0,100):
            return r.choice(group)
        return None


    def vaccinate(self, vaccine):
        """ Se vacuna a una persona """
        self.vaccine = vaccine
        self.doses += 1
        self.current_step_vaccine = 0
        # Se calcula el nivel de gravedad ahora que se ha vacunado
        self.set_severity()


    def set_severity(self):
        """ Se calcula el nivel de gravedad de una persona """
        # Si se tiene la vacuna 3 entonces no hay gravedad y se esta inmune
        if self.vaccine.vax_id == 3:
            self.is_severe = False
            self.severity = 0
            self.done = True
        else:
            # Se calcula la gravedad segun una formula creada
            multp = 1 + self.base_condition.gravity+self.base_illness.gravity
            cons = self.age_risk * multp
            pros = self.vaccine.immunity/2 * self.doses
            self.severity = cons - pros
            if self.base_condition.name == "Sin condicion" and self.base_illness.name == "Sin enfermedad":
                self.severity *= 0.4
            if self.severity >= 0.6:
                self.is_severe = True
            else:
                self.is_severe = False
            # Si se tiene la vacuna 2 entonces no se esta grave
            if self.vaccine.vax_id == 2:
                self.severity = 0
                self.is_severe = False


    # Metodos que fueron utilizados para ordenar a las personas por id
    # al momento de crear lo grupos de contactos
    def __gt__(self, other):
        if self._id > other._id:
            return True
        else:
            return False
    def __lt__(self, other):
        if self._id < other._id:
            return True
        else:
            return False
