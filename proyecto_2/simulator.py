# Simulador

# Se importan los modulos necesarios
import random as r
from disease import Disease
from community import Community

from vaccine import Vaccine

# Decorador para mostrar la informacion
def print_decorator(func):
    def wrap(text):
        print("="*80)
        func(text)
        print("")
    return wrap
@print_decorator
def print_w_format(text):
    print(text)


class Simulator():
    """ Clase dedicada al simulador """
    def __init__(self, community):
        # Se establece el simulador
        self.community = community
        # Enfermedad y vacunas a utilizar
        self.disease = self.community.disease
        self.v1 = Vaccine.new_vaccine1()
        self.v2 = Vaccine.new_vaccine2()
        self.v3 = Vaccine.new_vaccine3()
        self.vaxs = [self.v1, self.v2, self.v3]


    def run(self, steps):
        """ Ejecucion de la simulacion de contagios """

        # Se escoge una muestra al azar y se infecta
        limit = int(self.community.num_people * 0.03)
        patients = r.sample(self.community.hab_list, r.randint(1, limit))
        for patient in patients:
            patient.set_infected()
            patient.set_severity()

        # Reporte inicial de la comunidad
        self.community.init_report()

        # Comienza la simulacion por pasos
        nums = [0, 1, 2]
        for step in range(steps):
            print_w_format(f"step: {step+1}")

            # Se empieza a vacunar a partir del quinto paso
            if step >= 5:
                # 10 primeras dosis por paso
                daily_vaccinations = 10
                while True:
                    # Se selecciona el indice de vacuna a utilizar
                    try:
                        v_index = r.choice(nums)
                    except IndexError:
                        # Si no se puede entonces no hay mas primeras dosis
                        break
                    try:
                        # Se selecciona la persona para darle la primera dosis
                        person = r.choice(self.community.vax_groups[v_index])
                    except IndexError:
                        # Si no hay personas, se elimina la opcion de vacuna
                        nums.remove(v_index)
                        continue
                    # Vacuna a utilizar
                    vax = self.vaxs[v_index]
                    # Se coloca la primera dosis si corresponde
                    person.vaccinate(vax)
                    # Se remueve a la persona del grupo de primeras dosis
                    self.community.vax_groups[v_index].remove(person)
                    daily_vaccinations -= 1

                    # Se rompe si se alcanza el limite diario de vacunacion
                    if daily_vaccinations == 0:
                        break

            # Revision persona a persona de la comunidad
            for person in self.community.hab_list:
                # Se coloca la segunda dosis si corresponde, solo vacunas 1 y 2
                if 1 <= person.vaccine.vax_id <= 2:
                    person.current_step_vaccine += 1
                    if person.current_step_vaccine == person.vaccine.steps:
                        if person.doses < person.vaccine.num_doses:
                            person.doses += 1
                            person.set_severity()
                # Si la persona ya esta lista o grave no pasa nada
                if person.done:
                    continue
                if person.is_severe:
                    # Una persona grave igual puede morir
                    if person.severity >= r.randint(0, 100):
                        person.dies()
                        continue

                # Se revisa el estado de todos los enfermos
                if not person.healthy:
                    person.current_step_disease += 1
                    # La persona puede morir antes que la enfermedad acabe
                    if person.severity >= r.randint(0, 100):
                        person.dies()
                        continue

                    # Si la persona ya cumplio sus dias de enfermedad, se acaba
                    if person.current_step_disease == person.total_step_disease:
                        person.set_recovered()
                        continue

                    # Si la persona aun sigue enferma, puede contagiar
                    group = r.choice([person.family, person.contacts])
                    meet = person.encounter(group)
                    if meet:
                        if meet.healthy and not meet.done:
                            if self.disease.prob_infection*100 >= r.randint(0,100):
                                meet.set_infected()

            # Reporte de susceptibles, infectados y recuperados por cada paso
            self.community.report()
        # Reporte final de la comunidad
        self.community.final_report()
