# Guia 1 Unidad 2 - Explicacion

## integrantes
* Sebastián Bustamante Villarreal
* Bastián Morales

## La aplicacion general
Se tiene una aplicacion sencilla en la cual en una ventana se deben ingresar
el nombre de una persona, su ramo y su nota de aprobacion del 1 al 10.

Si la nota de aprobacion ingresada es menor 5, se comunica que se reprueba.
En caso contrario, se aprueba, y tambien se comunica, en una ventana de Dialogo
tipo "info"

Todo esto si el resto de los datos se ha ingresado de forma correcta.
Si alguno de los datos no se ha ingresado, se comunica en una ventana de dialogo
tipo error.


### La ventana About
la clase AboutDialog hereda de Gtk.AboutDialog.
Usa los metodos propios de la clase Gtk.AboutDialog que le permiten
definir informacion como autores, nombre de la aplicacion e icono, entre otros.

Tiene un metodo que le permite abrir el link que se muestra en la ventana de
de dialogo. La url se guarda como variable global de modo que el reciclaje de
codigo sea mas eficiente.

### La ventana principal
La clase MainWindow hereda de Gtk.Window
Usa metodos propios de Gtk.Window y otros metodos de clases padres, como
show_all.

La ventana principal usa una headerbar custom que tiene insertada el boton
para poder abrir la ventana About.

En la ventana tambien hay un objeto Gtk.Grid en el cual se contienen los demas
widgets, ordenados de forma que la aplicacion no se vea confusa.

Hay labels para indicar en que entrys se debe escribir el nombre del alumno
y el nombre del ramo respectivamente, y otra que sirve para indicar la
ComboBoxText en la que se debe escoger la nota.

En las entrys la unica manera en la que se pueda dar un error es si se deja un
caracter vacio "" o si se escriben espacios "    ", todas las demas entradas son
consideradas como validas.

Para la ComboBoxText dedicada a las notas la unica forma en la que se puede dar
error es si no se escoge nada, pues al inicial el objeto no hay ninguna opcion
seleccionada, de ahi en adelante, una vez se escoja algo, no se puede "volver
a seleccionar nada".
Teniendo en cuenta que este es un error que solamente puede ocurrir bajo una
condicion, se decidio que en vez de agregar una ventana que maneje este error,
la aplicacion iniciara con la primera opcion escogida. De ahi en adelante no se
puede "volver a escoger nada", por lo tanto no es posible que se de error.

HOLAAA
