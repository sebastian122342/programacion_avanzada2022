# messages para verficar decisiones

from gi.repository import Gtk


def on_error_clicked(widget, main, second):
    """ Dialogo de error asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.ERROR,
        buttons=Gtk.ButtonsType.CANCEL,
        text=main
    )
    dialog.format_secondary_text(
        second
    )
    dialog.run()
    dialog.destroy()


def on_info_clicked(widget, main, second):
    """ Dialogo de info asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.INFO,
        buttons=Gtk.ButtonsType.OK,
        text=main
    )
    dialog.format_secondary_text(
        second
    )
    dialog.run()
    dialog.destroy()


def on_question_clicked(widget, main, second):
    """ Dialogo de pregunta asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.QUESTION,
        buttons=Gtk.ButtonsType.YES_NO,
        text=main
    )
    dialog.format_secondary_text(
        second
    )
    response = dialog.run()
    if response == Gtk.ResponseType.YES:
        resp = True
    elif response == Gtk.ResponseType.NO:
        resp = False
    dialog.destroy()
    return resp
