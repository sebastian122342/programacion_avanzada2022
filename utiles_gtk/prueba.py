# prueba

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import urllib.request

class MainWindow(Gtk.Window):
    def __init__(self):
        super().__init__(title = "among us")
        self.set_default_size(600, 600)
        self.connect("destroy", Gtk.main_quit)

        self.box = Gtk.Box(spacing=6)
        self.add(self.box)

        self.hb = Gtk.HeaderBar()
        self.hb.set_title("hb")
        self.box.pack_start(self.hb, True, True, 0)

        self.button = Gtk.Button(label="holaa")
        self.button.connect("clicked", self.on_button_clicked)
        self.box.pack_start(self.button, True, True, 0)

        self.button_dialog = Gtk.Button(label="clic para dialogo")
        self.button_dialog.connect("clicked", self.abrir_dialogo)
        self.box.pack_start(self.button_dialog, True, True, 0)

        self.button_about = Gtk.Button(label="clic para aboutus")
        self.button_about.connect("clicked", self.on_button_dialogo)
        self.box.pack_start(self.button_about, True, True, 0)

    def on_button_clicked(self, btn):
        print("joder")

    def abrir_dialogo(self, btn):
        dialogo = DialogWindow(self)
        response = dialogo.run()
        if response == Gtk.ResponseType.CANCEL:
            print("se presiono cancel")
        elif response == Gtk.ResponseType.OK:
            print("se presiono ok")
        dialogo.destroy()

    def on_button_dialogo(self, btn):
        about = VentanaAbout()

class DialogWindow(Gtk.Dialog):
    def __init__(self, parent):
        super().__init__(title="Dialogo", transient_for=parent, flags=0)
        self.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OK, Gtk.ResponseType.OK
        )

        self.set_default_size(150, 100)

        label = Gtk.Label(label="hola este es el dialogo")

        box = self.get_content_area()
        box.add(label)
        self.show_all()

class VentanaAbout(Gtk.AboutDialog):
    def __init__(self):
        super().__init__(title="ABOUT US")
        autores = ["Red amongus", "Blue amongus"]
        section = "Autores"
        self.add_credit_section(section, autores)
        self.set_artists(["alva majo", "el pepe", "yellow amongus"])
        self.set_comments("me quiero matar")
        self.set_copyright("Todos los derechos reservados.")
        self.set_documenters(["Mis amigos", "Mi padre", "El jimmy"])
        self.set_license("Licencia de conducir")
        #self.set_license_type(Gtk.license)
        #self.set_logo()
        self.set_logo_icon_name("input-gaming")
        self.set_program_name("aplicacion siii")
        self.set_translator_credits("barack obama")
        self.set_version("2.0")
        self.set_website("https://www.youtube.com/user/alexelcapo")
        self.set_website_label("Importante")
        #queda algo de self.wrap_license(bool)
        self.connect("activate-link", self.abrir_link)

        self.show_all()

    def abrir_link(self, cosita, nose):
        urllib.request.urlopen(self.get_website())

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    Gtk.main()
