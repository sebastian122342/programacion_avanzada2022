"""
Guia 6 evaluada por Sebastián Bustamante Villarreal

"""

# Se importan los modulos necesarios
from plant import Plant
from estudiante import Estudiante
from rabano import Rabano
from hierba import Hierba
from trebol import Trebol
from violeta import Violeta

import random

def ask_number():
    # Pide un numero.
    number = input("Ingrese un numero: ")
    while not number.isdigit():
        number = input("Ingrese un numero: ")
    return int(number)

class Garden():
    """ Clase Garden que contiene interacciones entre clases """
    def __init__(self):
        # Se tiene una coleccion de objetos Estudiante y una matriz con plantas
        self._estudiantes = []
        self._plantas = [[], []]

    # Getter de la coleccion de estudiantes
    @property
    def estudiantes(self):
        return self._estudiantes

    # Setter de la coleccion de estudiantes
    @estudiantes.setter
    def estudiantes(self, estudiante):
        if isinstance(estudiante, Estudiante):
            self._estudiantes.append(estudiante)
        else:
            print("Tipo de dato no valido")

    # Getter de la matriz de plantas
    @property
    def mesa_plantas(self):
        return self._plantas

    def plantas(self, estudiante):
        # Se retorna la lista de plantas de un estudiante
        return estudiante.plantas

    def full_garden(self):
        # Se muestra la situación completa
        total = ""
        print("[ventana]"*3)
        for fila in self.mesa_plantas:
            for planta in fila:
                total += planta.nombre[0]
            total += "\n"
        print(total)

    def crear_alumnos(self):
        # Se crean 12 objetos tipo Estudiante y se agregan a la lista
        nombres = ["Alicia", "Marit", "Pepito", "David", "Eva", "Lucia",
                    "Rocı́o", "Andrés", "José", "Belen", "Sergio", "Larry"]
        nombres.sort()
        for nombre in nombres:
            self.estudiantes = Estudiante(nombre)

    def crear_plantas(self):
        # Se crean las plantas y se colocan en la matriz mesa.
        # Se aprovecha de asignar estudiante dueño a la vez que se crea y se
        # coloca en la mesa.
        for fila in self.mesa_plantas:
            for estudiante in self.estudiantes:
                for k in range(2):
                    tipo = random.randint(1, 4)
                    if tipo == 1:
                        planta = Rabano(estudiante)
                    elif tipo == 2:
                        planta = Hierba(estudiante)
                    elif tipo == 3:
                        planta = Trebol(estudiante)
                    elif tipo == 4:
                        planta = Violeta(estudiante)
                    fila.append(planta)
                    estudiante.plantas = planta

    def consultar_estudiante(self):
        # Se consulta la informacion de plantas de un solo estudiante
        while True:
            print("\nConsulta sobre estudiantes.")
            for x in range(len(self.estudiantes)):
                print(f"{x+1}. {self.estudiantes[x].nombre}")
            print(f"{len(self.estudiantes)+1}. Salir")
            print("\nQue desea consultar? ingrese el numero del estudiante.")
            # Se verifica que la opcion de estudiante que se escoja sea valida
            opcion = ask_number()
            while not 1 <= opcion <= len(self.estudiantes)+1:
                opcion = ask_number()
            # Una vez escogido el estudiante, se muestra su informacion
            if  1 <= opcion < len(self.estudiantes)+1:
                estudiante_obj = self.estudiantes[opcion-1]
                print("\n"+"[ventana]"*3)
                total = ""
                for fila in self.mesa_plantas:
                    for plant in fila:
                        if plant.estudiante is estudiante_obj:
                            total += plant.nombre[0]
                        else:
                            total += "*"
                    total += "\n"
                print(total)

                # Se muestra ademas "con palabras" las plantas del estudiante
                total = f"\nA {estudiante_obj.nombre} le pertenecen "
                for planta in self.plantas(estudiante_obj):
                    total += f"{planta.nombre} "
                print(total)
            else:
                break


garden = Garden()
garden.crear_alumnos()
garden.crear_plantas()
garden.full_garden()
garden.consultar_estudiante()
