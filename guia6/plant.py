
class Plant():
    """ Clase Plant que sera la clase madre de las demas plantas """
    def __init__(self, estudiante):
        # Un objeto planta le pertenece a un objeto estudiante
        self._estudiante = estudiante

    # getter del estudiante
    @property
    def estudiante(self):
        return self._estudiante
