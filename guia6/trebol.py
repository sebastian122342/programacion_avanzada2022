
from estudiante import Estudiante
from plant import Plant

class Trebol(Plant):
    """ Clase Trebol hija de la clase Plant """
    def __init__(self, estudiante):
        # Tiene las caracteristicas de Plant + un nombre distintivo
        super().__init__(estudiante)
        self._nombre = "Trebol"

    # getter del nombre
    @property
    def nombre(self):
        return self._nombre
