
from plant import Plant

class Estudiante():
    """ Clase Estudiante """
    def __init__(self, nombre):
        # Un objeto estudiante tiene un nombre y una coleccion de objetos Plant
        self._nombre = nombre
        self._plantas = []

    # getter del nombre
    @property
    def nombre(self):
        return self._nombre

    # getter de la coleccion de plantas
    @property
    def plantas(self):
        return self._plantas

    # Setter de la coleccion de plantas
    @plantas.setter
    def plantas(self, planta):
        if isinstance(planta, Plant):
            self._plantas.append(planta)
        else:
            print("Tipo de dato no valido")
