# About Dialog

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class AboutDialog(Gtk.AboutDialog):
    """ Ventana de dialogo con metodos para mostrar informacion necesaria """
    def __init__(self):
        super().__init__(title="Dialogo About")
        self.set_modal(True)
        autores = ["Sebastián Bustamante", "Bastián Morales"]
        self.add_credit_section("Autores", autores)
        self.set_comments("Primera Guia Unidad 2, Programacion Avanzada")
        self.set_logo_icon_name("input-gaming")
        self.set_program_name("Cuenta Bancaria")
        self.set_version("1.0")

        self.show_all()
