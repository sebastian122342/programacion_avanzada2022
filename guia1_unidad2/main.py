"""
Guia 1 Unidad 2 Programacion Avanzada

Por Sebastián Bustamante y Bastián Morales

Se crea una aplicacion donde se resuelve un problema sencillo.
Se abre una ventana en la cual se tienen que ingresar datos, para poder
ingresar o retirar cierta cantidad de dinero. Se tienen encuenta las reglas
de negocio pertinentes.
"""

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked
from messages import on_info_clicked
from messages import on_question_clicked

# Lista que contiene los nombres de los bancos a usar
bancos = ["Banco Santander", "Banco Itau", "Banco Bci",
            "Banco Estado", "Banco de Chile", "Banco Utal", "Banco Scotiabank"]

class MainWindow(Gtk.Window):
    def __init__(self):
        # Se establece la ventana principal
        super().__init__()
        self.set_default_size(600, 600)
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la grid que contendrá los demas widgets
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Ingreso retiro de banco")
        self.hb.set_subtitle("Guia 1 Unidad 2 - Programacion Avanzada")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Label para la Comboboxtext de banco
        self.lbl_banco = Gtk.Label(label="Banco:")
        self.grid.attach(self.lbl_banco, 0 , 0 , 1, 1)

        # Comboboxtext de banco
        self.combo_banco = Gtk.ComboBoxText()
        for banco in bancos:
            self.combo_banco.append_text(banco)
        self.grid.attach(self.combo_banco, 1, 0, 1, 1)
        self.combo_banco.set_active(0)

        # Label para el autor de la transaccion
        self.lbl_autor = Gtk.Label(label="Autor:")
        self.grid.attach(self.lbl_autor, 0 , 1 , 1, 1)

        #Entry para el nombre del autor
        self.entry_autor =  Gtk.Entry()
        self.entry_autor.set_placeholder_text("Autor de la transaccion")
        self.grid.attach(self.entry_autor, 1, 1, 1, 1)

        # Label de la entry de monto
        self.lbl_monto = Gtk.Label(label="Monto:")
        self.grid.attach(self.lbl_monto, 0 , 2 , 1, 1)

        # Entry para el monto
        self.entry_monto =  Gtk.Entry()
        self.entry_monto.set_placeholder_text("Monto")
        self.grid.attach(self.entry_monto, 1, 2, 1, 1)

        # Label que acompaña al dinero total
        self.lbl_dinero = Gtk.Label(label="Dinero total:")
        self.grid.attach(self.lbl_dinero, 0 , 3 , 1, 1)

        # Label que acumula el dinero total
        self.dinero = Gtk.Label(label="1000")
        self.grid.attach(self.dinero, 1, 3, 1, 1)

        # Boton para ingresar dinero
        self.btn_ingresa = Gtk.Button(label="Ingresar")
        self.btn_ingresa.connect("clicked", self.procesar)
        self.grid.attach(self.btn_ingresa, 0, 4, 1, 1)

        # Boton para retirar dinero
        self.btn_retira = Gtk.Button(label="Retirar")
        self.btn_retira.connect("clicked", self.procesar)
        self.grid.attach(self.btn_retira, 1, 4, 1, 1)

    def procesar(self, btn=None):
        """
        Se procesa la informacion ingresada.
        El procesado es comun para ingresar o retirar dinero, pues
        sin importar que se vaya a hacer, se debe ingresar un autor y una
        cantidad numerica entera de dinero.

        La accion que se ejecute luego del revisado inicial dependera del
        boton que haya llamado al metodo procesar.
        """

        # Se confirma si es que el usuario quiere retirar/ingresar
        principal = f"{btn.get_label()} dinero"
        secundaria = f"¿Seguro que quiere {btn.get_label()} el monto?"
        if not on_question_clicked(self, principal, secundaria):
            return

        # Se hace la verificacion comun a ingresar y retirar
        autor = self.entry_autor.get_text().strip(" ")
        monto = self.entry_monto.get_text().strip(" ")
        dinero = int(self.dinero.get_label())
        banco =  self.combo_banco.get_active_text()

        # El autor no debe ser "" y el monto debe ser un numero entero
        if autor == "" or not monto.isnumeric():
            principal = "Error al ingresar datos"
            secundaria = "El monto debe ser un numero y debe ingresar nombre"
            if autor == "":
                self.entry_autor.set_text("")
            if not monto.isnumeric():
                self.entry_monto.set_text("")
            on_error_clicked(self, principal, secundaria)
            return

        if btn is self.btn_ingresa:
            # Si el boton corresponde a ingresar, el monto se suma al saldo
            self.dinero.set_label(str(dinero+int(monto)))
        else:
            # Si el boton corresponde a retirar, se resta el monto del saldo
            if int(monto) > dinero:
                # No se puede retirar si el monto es mayor al saldo
                principal = "Error al retirar dinero"
                secundaria = "El monto a retirar debe ser como maximo el saldo"
                on_error_clicked(self, principal, secundaria)
                self.entry_monto.set_text("")
                return
            self.dinero.set_label(str(dinero-int(monto)))

        # Se muestra en una ventana de dialogo tipo info las acciones hechas
        principal = f"{btn.get_label()} dinero"
        secundaria = f"{autor} decidio {btn.get_label()} ${monto} desde {banco}"
        on_info_clicked(self, principal, secundaria)
        # Se resetean los datos ya ingresados
        self.entry_autor.set_text("")
        self.entry_monto.set_text("")
        self.combo_banco.set_active(0)

    def on_btn_about(self, btn):
        """ Se llama muestra la ventana About """
        AboutDialog()

if __name__ == "__main__":
    window = MainWindow()
    window.show_all()
    Gtk.main()
