# Guia 1 Unidad 2 Programacion Avanzada

## Integrantes
* Sebastián Bustamante
* Bastián Morales

## Ventana Principal
La clase Ventana Principal hereda todas las caracteristicas de Gtk.Window.
Se utiliza super() para poder llamar al constructor de la clase madre
(Gtk.Window) e inicializar correctamente el objeto.

Se utilizan metodos para poder definir ciertas caracteristicas esteticas
como el tamaño de la ventana, sus margenes y un evento que acaba con el programa
cuando se cierra la ventana principal.

### HeaderBar
El objeto Gtk.Window viene con una TitleBar predeterminada. Esta puede ser
reemplazada por un objeto tipo HeaderBar para tener una TitleBar custom.
Se crea una HeaderBar a la cual se le agrega un titulo y subtitulo, asi como
también el Boton que abre la ventana About. Se establece mediante un metodo que
se muestren los botones de minimizar, maximizar y cerrar.

### Grid
Para manejar los demas widgets que se utilizan en la aplicacion, se instancio un
objeto tipo Gtk.grid. En este ubican los objetos en diferentes posiciones segun
se estime conveniente. Por temas de estetica, tambien se establece un espaciado
entre columnas y filas de la grilla.

### Labels
Se utilizan objetos tipo Gtk.Label para mostrar piezas de texto que indiquen
informacion importante. Para el caso de la aplicacion, indican la cantidad de
dinero que se tiene y para que utilizar cada widget.

### Entrys
Se utilizan objetos tipo Gtk.Entry para recibir informacion necesaria. Esta
informacion es luego procesada estableciendo reglas de negocio: El monto debe
ser un numero entero (comprobado con isnumeric()) y el nombre puede ser
cualquier string que no sea un caracter vacio "" o espacios "   ".

Si se desea depositar solamente se deben cumplir las condiciones anteriores.
Si se desea retirar hay una regla de negocio adicional: La cantidad a retirar
no puede ser mayor que la cantidad de dinero que ya estaba en la cuenta.

### ComboBoxText
Se utiliza un objeto tipo Gtk.ComboBoxText para hacer que el usuario ingrese
desde que banco hara la transaccion. Se prefiere una Gtk.ComboBoxText por sobre
otros widgets como Gtk.Entry ya que el manejo de excepciones para el primero
es mas sencillo. De hecho, no es necesario ningun tipo de manejo, pues la
aplicacion inicializa con la primera opcion preseleccionada. De ahi en adelante,
no se puede "volver a escoger nada", asi que aunque el usuario no interactue con
el widget,siempre habra una opcion valida escogida.


### Buttons
En la Ventana Principal se manejan 3 objetos tipo Gtk.Button.
Uno que esta inserto en la HeaderBar y que se encarga de abrir la Ventana About,
y los otros dos son los encargados de ingresar/retirar dinero.

* El boton para la ventana About tiene un icono de stock asignado y un evento
para que cuando se clickee se abra dicha ventana.
* Los botones ingresar/retirar son similares, tienen labels que indican para que
sirven y, cuando son clickeados, llaman al mismo metodo: procesar().

#### Metodo procesar()
Tal como indica su nombre, procesar() se encarga de recibir la informacion
ingresada en los widgets de la ventana, revisar si es que hay errores y, en caso
de que no los haya y dependiendo del boton que se haya clickeado, ingresar
o retirar el dinero, no sin antes manejar la excepcion ya mencionada al retirar.

Se usa el mismo metodo para ambos botones ya que aunque se ingrese o retire
dinero siempre se debe hacer un chequeo de informacion comun: que el monto sea
un entero y que el nombre no sea "" o espacios " ".

El metodo procesar recibe 2 argumentos, self y btn. btn se refiere al boton
especifico que desencadeno el evento. A partir de btn se puede diferenciar
que boton (ingresar o retirar) desencadeno el evento y condicionar el
comportamiento del metodo.

## Ventana About
La clase About hereda todos los elementos de Gtk.AboutDialog. Al igual que con
la Ventana Principal, se utiliza super() para poder llamar al constructor de la
clase madre e inicializar correctamente el objeto.
Se utilizan metodos propios de Gtk.AboutDialog para determinar la informacion
que se considera relevante mostrar, como los autores, el nombre de la app,
comentarios, entre otros.
Tambien se establece un icono de stock para la aplicacion.

## Mensajes
El archivo messages.py contiene funciones que se encargan de generar ventanas
de dialogo tipo message (Gtk.MessageDialog). Estas sirven para pedir
confirmacion de una decision, comunicar un error o simplemente informar.
Para el caso de la aplicacion, la Ventana madre de todos estos MessageDialog
es la ventana principal.

En todos estos dialogos de mensaje se muestran dos cadenas de texto, "principal"
y "secundaria" que sirven para indicarle al usuario en que consiste la ventana
de dialogo que se le esta mostrando.

* En on_error_clicked se muestra un dialogo tipo ERROR con un boton tipo CANCEL
* En on_info_clicked se muestra un dialogo tipo INFO con un boton tipo OK
* en on_question_clicked se muestra un dialogo tipo QUESTION con botones YES y
NO. Se retorna la respuesta del usuario con un Booleano (True o False)
