# clase champion generica que se puede usar para hacer lo mismo de otras maneras

from interfaz_champion import Champion_plano
from multipledispatch import dispatch

class Champion(Champion_plano):
    def __init__(self, nombre, salud, ataque, defensa, resistencia_magia):
        self._nombre = nombre
        self._salud = salud
        self._ataque = ataque
        self._defensa = defensa
        self._magia = 0
        self._resistencia_magia = resistencia_magia

    @property
    def salud(self):
        return self._salud
    @salud.setter
    def salud(self, value):
        if isinstance(value, int):
            self._salud = value

    @property
    def ataque(self):
        return self._ataque

    @property
    def defensa(self):
        return self._defensa

    @property
    def magia(self):
        return self._magia
    @magia.setter
    def magia(self, value):
        if isinstance(value, int):
            self._magia = value

    @property
    def resistencia_magia(self):
        return self._resistencia_magia

    @property
    def nombre(self):
        return self._nombre

    # Metodos que se ponen porque asi vienen en la interfaz

    @dispatch(Champion)
    def atacar(self, obj):
        damage = self.ataque - self.defensa
        if damage < 0:
            damage = 0
        obj.salud = obj.salud - self.ataque
        print(f"{self.nombre} ha dañado a {self.obj} por {damage} de daño")

    @dispatch(Champion, str)
    def atacar(self, obj, letra_habilidad):
        opciones = ["Q", "W", "E", "R"]
        if not letra_habilidad in opciones:
            return

        if letra_habilidad == "Q":
            self.magia = danio_Q
        elif letra_habilidad == "W":
            self.magia = danio_W
        elif letra_habilidad == "E":
            self.magia = danio_E
        elif letra_habilidad == "R":
            self.magia = danio_R

        print(f"{self.nombre} tiro la {letra_habilidad} a {obj.nombre}")
        damage = self.magia - obj.resistencia_magia
        if damage < 0:
            damage = 0
        obj.salud = obj.salud - damage
        print(f"{self.nombre} lanzo la {letra_habilidad} a {obj.nombre} por {damage} de daño")

    def comprobar_estado(self):
        if self.salud <= 0:
            print(f"{self.nombre} ha muerto")
