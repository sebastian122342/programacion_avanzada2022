# interfaz ejercicio en clase

from abc import abstractmethod
from abc import ABCMeta

class Champion_plano(metaclass=ABCMeta):

    @abstractmethod
    def atacar(self, obj, letra):
        pass

    @abstractmethod
    def comprobar_estado(self):
        pass
