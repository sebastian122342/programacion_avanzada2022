# Dialogo para hacer reservas

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk


class DialogBook(Gtk.Dialog):
    """ Ventana de dialogo para decidir informacion sobre la estadia """
    def __init__(self, data):
        # Se establece la ventana de dialogo
        super().__init__(title="Datos sobre la estadia")
        self.set_modal(True)
        self.set_default_size(400, 400)

        button = self.add_button(Gtk.STOCK_OK, Gtk.ResponseType.OK)
        button.props.always_show_image = True
        button = self.add_button(Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)
        button.props.always_show_image = True
        
        self.data = data

        # Se establece la grid donde se colocara la ComboBoxText
        self.box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.box.pack_start(self.grid, True, True, 1)

        # Label para la entry del nombre de quien hacer la reserva
        self.lbl_author = Gtk.Label(label="Titular reserva")
        self.grid.attach(self.lbl_author, 0, 0, 1, 1)

        # Entry autor de la reserva
        self.entry_author = Gtk.Entry()
        self.entry_author.set_placeholder_text("Ingresar el titular")
        self.grid.attach_next_to(self.entry_author, self.lbl_author,
                                Gtk.PositionType.RIGHT, 1, 1)

        # Label entry nombre de hotel
        self.lbl_hotel = Gtk.Label(label="Hotel")
        self.grid.attach_next_to(self.lbl_hotel, self.lbl_author,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # Entry no editable con el hotel escogido
        self.entry_hotel = Gtk.Entry()
        self.entry_hotel.set_text(self.data["name"])
        self.entry_hotel.props.editable = False
        self.grid.attach_next_to(self.entry_hotel, self.lbl_hotel,
                                Gtk.PositionType.RIGHT, 1, 1)

        # Label de precio
        self.lbl_price = Gtk.Label(label="Precio")
        self.grid.attach_next_to(self.lbl_price, self.lbl_hotel,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # Entry no editable con el precio
        self.entry_price = Gtk.Entry()
        self.entry_price.set_text(self.data["price"])
        self.entry_price.props.editable = False
        self.grid.attach_next_to(self.entry_price, self.lbl_price,
                                Gtk.PositionType.RIGHT, 1, 1)

        # Label para la comboboxtext de cantidad de dias a quedarse
        self.lbl_days = Gtk.Label(label="Cantidad de dias")
        self.grid.attach_next_to(self.lbl_days, self.lbl_price,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # ComboBoxText para la cantidad de dias a quedarse
        self.combo_days = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.combo_days.append_text(str(x))
        self.combo_days.set_active(0)
        self.combo_days.connect("changed", self.on_combo_changed)
        self.grid.attach_next_to(self.combo_days, self.lbl_days,
                                Gtk.PositionType.RIGHT, 1, 1)

        # Label para indicar el precio total
        self.lbl_total = Gtk.Label(label="Total")
        self.grid.attach_next_to(self.lbl_total, self.lbl_days,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # Entry no editable para indicar el precio total
        self.entry_total = Gtk.Entry()
        self.entry_total.set_text(self.data["price"])
        self.entry_total.props.editable = False
        self.grid.attach_next_to(self.entry_total, self.lbl_total,
                                Gtk.PositionType.RIGHT, 1, 1)

        self.show_all()


    def on_combo_changed(self, combo=None):
        """ Se actualiza el precio total segun cambia la cantidad de dias """
        price = int(self.data["price"])
        days = int(combo.get_active_text())
        total = str(price * days)
        self.entry_total.set_text(total)
