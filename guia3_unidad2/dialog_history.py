# Dialogo historial de reservas

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

import pandas as pd

file = "./history.csv"


class History(Gtk.Dialog):
    """ Ventana de dialogo de historial de reservas hechas """
    def __init__(self):
        # Se establece la ventana
        super().__init__(title="Historial de reservas")
        self.set_modal(True)

        # Se establece la grid donde se colocara el ScrolledWindow
        self.box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.box.pack_start(self.grid, True, True, 1)

        # ScrolledWindow
        self.scroll = Gtk.ScrolledWindow()
        self.grid.attach(self.scroll, 0, 0, 25, 20)

        # TreeView
        self.tree = Gtk.TreeView()
        self.scroll.add(self.tree)

        # Se carga la informacion del archivo al treeview
        self.cargar_del_csv(file)

        self.show_all()

    def cargar_del_csv(self, file, box=None):
        """ Se carga la info del csv a un treeview """
        # Se lee el archivo y se arma un dataframe
        self.data = pd.read_csv(file)

        # Si ya hay data en el treeview, se quitan las columnas
        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)

        # Se construye el modelo
        largo_columnas = len(self.data.columns)
        model = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree.set_model(model=model)

        # Se colocan las columnas
        cell = Gtk.CellRendererText()
        for item in range(len(self.data.columns)):
            column = Gtk.TreeViewColumn(self.data.columns[item],
                                        cell, text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)

        # Se rellena el tree con la data
        for item in self.data.values:
            line = [str(x) for x in item]
            model.append(line)
