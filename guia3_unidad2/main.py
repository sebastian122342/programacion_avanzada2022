"""
Guia 3 Unidad 2 - Programacion Avanzada
Por Sebastián Bustamante y Bastián Morales
Se resuelve una problematica sencilla mediante la creacion de una aplicacion
que utiliza interfaces graficas.
Se tiene un catalogo de hoteles, se puede escoger en que hotel hacer una
reserva y por cuantos dias, con un maximo de 10.
La informacion de la reserva es almacenada en otro archivo, que funciona como
historial. Se puede acceder al historial desde la misma aplicacion.
"""

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog
from dialog_book import DialogBook
from dialog_history import History

from messages import on_error_clicked
from messages import on_question_clicked
from messages import on_info_clicked

import pandas as pd

main_file = "./hotel.csv"
second_file = "./history.csv"


def create_line(list):
    """ Toma una lista y genera una linea para el archivo csv """
    # la lista tiene el orden autor-hotel-dias-precio_total
    # Si algun elemento tiene "," en su interior, se agregan comillas
    for x in range(len(list)):
        list[x] = str(list[x])
        if "," in list[x]:
            list[x] = f'"{list[x]}"'
    line = ",".join(list) + "\n"
    return line


def add_data(line, file):
    """ Agrega la linea deseada a un archivo """
    with open(file, "a") as file:
        file.write(line)


class MainWindow(Gtk.Window):
    """ Ventana principal de la aplicacion """
    def __init__(self):
        # Se establece la ventana
        super().__init__()
        self.set_default_size(600, 600)
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Catalogo de hoteles")
        self.hb.set_subtitle("Guia 3 Unidad 2 - Programacion Avanzada")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Boton historial
        self.btn_history = Gtk.Button()
        icon = Gio.ThemedIcon(name="dialog-information")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_history.add(image)
        self.btn_history.connect("clicked", self.on_btn_history)
        self.hb.pack_start(self.btn_history)

        # Gtk grid
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Scrolled window para el treeview
        self.scroll_tree = Gtk.ScrolledWindow()
        self.grid.attach(self.scroll_tree, 0, 0, 20, 30)

        # Scrolled window para la promo
        self.scroll_promo = Gtk.ScrolledWindow()
        self.grid.attach_next_to(self.scroll_promo, self.scroll_tree,
                                Gtk.PositionType.RIGHT, 15, 10)

        # Boton de checkout
        self.btn_checkout = Gtk.Button()
        icon = Gio.ThemedIcon(name="add")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        aux_box = Gtk.Box()
        aux_box.set_spacing(5)
        aux_box.pack_start(image, True, True, 1)
        aux_box.pack_start(Gtk.Label(label="Hacer reserva"), True, True, 1)
        self.btn_checkout.add(aux_box)
        self.btn_checkout.connect("clicked", self.on_btn_checkout)
        self.grid.attach_next_to(self.btn_checkout, self.scroll_promo,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # Treeview
        self.tree = Gtk.TreeView()
        self.tree.connect("row-activated", self.tree_row_activated)
        self.tree.set_activate_on_single_click(True)
        self.scroll_tree.add(self.tree)

        # Label promo
        self.lbl_promo = Gtk.Label()
        self.lbl_promo.set_line_wrap(True)
        self.lbl_promo.set_halign(Gtk.Align.START)
        self.lbl_promo.set_valign(Gtk.Align.START)
        self.scroll_promo.add(self.lbl_promo)

        self.cargar_del_csv(main_file)
        self.show_all()


    def cargar_del_csv(self, file, box=None):
        """ Se carga la info del csv a un treeview """
        # Se lee el archivo y se arma un dataframe
        self.data = pd.read_csv(file)

        # Si ya hay data en el treeview, se quitan las columnas
        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)

        # Se construye el modelo
        largo_columnas = len(self.data.columns)
        model = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree.set_model(model=model)

        # Se colocan las columnas
        cell = Gtk.CellRendererText()
        for item in range(len(self.data.columns)):
            column = Gtk.TreeViewColumn(self.data.columns[item],
                                        cell, text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)
            if item == 3:
                column.set_visible(False)

        # Se rellena el tree con la data
        for item in self.data.values:
            line = [str(x) for x in item]
            model.append(line)


    def tree_row_activated(self, model, path, iter):
        """ Se carga la promo a un label """
        model, it = self.tree.get_selection().get_selected()
        if model is None:
            return False
        text = "<b>Promo:</b>\n\n"
        text = f"{text}{model.get_value(it, 3)}"
        # se agrega el texto a la label de promo
        self.lbl_promo.set_markup(text)


    def on_btn_checkout(self, btn=None):
        """ Se agrega la reserva al historial """
        # Se verifica que se haya escogido una fila
        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            on_error_clicked(self, "Error", "No se ha seleccionado fila")
            return

        # Se pregunta si de verdad se desa hacer la reserva
        main = "Hacer reserva"
        second = "¿Seguro desea hacer la reserva?"
        if not on_question_clicked(self, main, second):
            return

        # Se guarda la info de la fila en un diccionario
        dict = {}
        for x in range(self.tree.get_n_columns()):
            column_name = self.tree.get_column(x).get_title()
            dict[column_name] = model.get_value(it, x)

        # Se levanta la ventana de dialogo, con la info de la fila
        dialog = DialogBook(dict)
        resp = dialog.run()
        if resp == Gtk.ResponseType.OK:
            # Se determinan los datos ingresados
            author = dialog.entry_author.get_text().strip(" ").title()
            days = dialog.combo_days.get_active_text()
            total_price = int(dialog.entry_price.get_text())*int(days)

            # Verificacion de errores, se debe ingresar un autor de la reserva
            if not author:
                on_error_clicked(dialog, "Error", "Se debe ingresar autor")
                dialog.destroy()
                return

            # Se agrega la info al archivo de historial de reservas
            list = [author, dict["name"], days, total_price]
            add_data(create_line(list), second_file)
            on_info_clicked(dialog, "Exito", "Se he hecho la reserva")
        dialog.destroy()


    def on_btn_history(self, btn=None):
        """ Se muestra el dialogo del historial de reservas """
        History()


    def on_btn_about(self, btn=None):
        """ Se muestra la ventana About """
        AboutDialog()


if __name__ == "__main__":
    window = MainWindow()
    Gtk.main()
