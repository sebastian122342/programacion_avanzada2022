# Guia 3 Unidad 2 - Programacion Avanzada
Se crea una aplicacion que resuelve una problematica sencilla.
La aplicacion se encarga de mostrar un catalogo de hoteles disponibles,
y se puede hacer una reserva por hasta 10 dias.
Luego de hacer la reserva, esta se almacena en otro archivo que funciona como
historial, se puede acceder a este archivo desde la misma aplicacion.

## Autores
* Sebastián Bustamante
* Bastián Morales

## Ventana Principal
La clase MainWindow hereda del objeto Gtk.Window, funciona como la clase
principal de la aplicacion. Contiene una Gtk.Grid para almacenar y ordenar
los widgets necesarios para su funcionamiento.

Tambien tiene un objeto tipo Gtk.HeaderBar que sobreescribe a la titlebar que ya
viene incorporada en el objeto Gtk.Window original. A esta HeaderBar se le
agrega un titulo, subtitlo, boton de informacion para abrir el historial de
reservas y boton para abrir la ventana About.

![main_window](./pics/main_window.png)

## Agregar reserva
Para poder agregar una reserva y que luego esta sea almacenada en el archivo
de historial se hace uso de una ventana de dialogo DialogBook que hereda de
Gtk.Dialog y contiene los widgets necesarios para ingresar informacion nueva.

En esta ventana se hace uso de un objeto tipo Gtk.ComboBoxText que hereda de
Gtk.ComboBox para poder escoger la cantidad de dias a hospedarse, de 1 a 10.

![dialog_book](./pics/dialog_book.png)

## Uso de TreeView
En la aplicacion se usan objetos tipo Gtk.TreeView en dos ocasiones. Primero
para mostrar el catalogo de hoteles y luego para mostrar el historial de
reservas hechas.

Catalogo de hoteles

![hotel_catalog](./pics/hotel_catalog.png)

Historial de reservas

![history](./pics/history.png)

El objeto TreeView permite mostrar la informacion del archivo csv de manera
ordenada y facil de entender. El traspaso de la informacion del archivo csv
al objeto tipo Gtk.Treeview se hace mediante el metodo de clase cargar_del_csv

### Cargar del csv
El metodo cargar_del_csv genera un dataframe a partir de un archivo csv usando
pandas, con read_csv. El dataframe ordena la informacion contenida en el csv y
permite que sea de facil acceso, similar a un diccionario.

El metodo permite traspasar la informacion del archivo csv al TreeView, y se
puede utilizar tanto para la primera vez que se abre el archivo como para todas
las veces en las que se decide agregar o eliminar informacion, para refrescar la
pantalla.

    # Si ya hay data en el treeview, se quitan las columnas
    if self.tree.get_columns():
        for column in self.tree.get_columns():
            self.tree.remove_column(column)

Luego se necesita definir un modelo para el TreeView el cual sera una ListStore.
En la ListStore se debe especificar cuantas columnas se tendran y de que tipo,
para este casos son todas string.

    # Se construye el modelo
    largo_columnas = len(data.columns)
    modelo = Gtk.ListStore(*(largo_columnas * [str]))
    self.tree.set_model(model=modelo)

Despues de definir el modelo se establecen las columnas en el TreeView. Los
nombres de las columnas del TreeView seran las mismas que las del dataframe.

    # Se colocan las columnas
    cell = Gtk.CellRendererText()
    for item in range(len(data.columns)):
        column = Gtk.TreeViewColumn(data.columns[item], cell, text=item)
        self.tree.append_column(column)
        column.set_sort_column_id(item)
        # Se oculta la columna que no sera mostrada
        if item == 3:
            column.set_visible(False)

Para la informacion del catalogo, se oculta la columna 3 pues sus contenidos
seran mostrados en la label del lado derecho de la aplicacion, correspondiente
a la el texto que el local tiene para promocionarse a si mismo.

En el caso del historial, no se ocultan columnas.

Finalmente se termina de llenar el TreeView con la informacion, accediendo a
los valores del dataframe

    # Se rellena el tree con la data
    for item in data.values:
        line = [str(x) for x in item]
        modelo.append(line)

## Ventana About
La clase AboutDialog hereda de Gtk.AboutDialog y utiliza los metodos propios
de la clase padre para poder definir informacion relevante de manera sencilla.
Se definen nombre de los autores, nombre del programa, version, icono, etc.

    def __init__(self):
      super().__init__(title="About Us")
      self.set_modal(True)
      self.add_credit_section("Autores", authors)
      self.set_comments(comment)
      self.set_logo_icon_name(icono_stock)
      self.set_program_name(program_name)
      self.set_version("1.0")

## Mensajes
En la aplicacion se utilizan ventanas de dialogo de mensaje de diferentes tipos,
info, pregunta y error. Sirven para poder dar informacion de manera rapida y
sencilla al usuario.

![on_error_clicked](./pics/on_error_clicked.png) ![on_question_clicked](./pics/on_question_clicked.png) ![on_info_clicked](./pics/on_info_clicked.png)
