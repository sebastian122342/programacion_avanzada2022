#ejercicio 5

class Estudiante():
    def __init__(self, carrera, puntaje, nombre):
        self._carrera = carrera
        self._puntaje = puntaje
        self._nombre_estudiante = nombre
        self._asignaturas = []

    @property
    def carrera(self):
        return self._carrera

    @property
    def puntaje(self):
        return self._puntaje

    @property
    def nombre_estudiante(self):
        return self._nombre_estudiante

    @property
    def asignaturas(self):
        return self._asignaturas

    @carrera.setter
    def carrera(self, carrera):
        self._carrera = carrera

    @puntaje.setter
    def puntaje(self, valor):
        self._puntaje = puntaje

    @nombre_estudiante.setter
    def nombre_estudiante(self, nombre):
        self._nombre_estudiante = nombre

    @asignaturas.setter
    def asignaturas(self, nombre_asignatura):
        if not nombre_asignatura in self._asignaturas:
            self._asignaturas.append(nombre_asignatura)
        else:
            print("No se ha agregado la asignatura porque ya esta en la lista")
            

class Carrera():
    def __init__(self, nombre_carrera):
        self._list_estudiantes = []
        self._nombre = nombre_carrera

    @property
    def lista_estudiantes(self):
        return self._list_estudiantes

    @property
    def nombre_carrera(self):
        return self._nombre

    @lista_estudiantes.setter
    def lista_estudiantes(self, estudiante):
        self._list_estudiantes.append(estudiante)


class Asignatura():
    def __init__(self, nombre):
        self._nombre = nombre

    @property
    def nombre(self):
        return self._nombre

    @nombre.setter
    def nombre(self, nombre_asignatura):
        self._nombre = nombre_asignatura


if __name__ == "__main__":
    # se crea una carrera y 3 estudiantes
    carrerax = Carrera("Bioinformatica")

    estudiantex = Estudiante(carrerax, 850, "Pedrito")
    estudiantey = Estudiante(carrerax, 720, "Manuel")
    estudiantez = Estudiante(carrerax, 450, "Isaias")

    # se crean 3 asignaturas
    asignaturax = Asignatura("Calculo II")
    asignaturay = Asignatura("Programacion avanzada")
    asignaturaz = Asignatura("Probabilidad y estadistica")

    # se agregan los 3 estudiantes a la carrera x
    carrerax.lista_estudiantes = estudiantex
    carrerax.lista_estudiantes = estudiantey
    carrerax.lista_estudiantes = estudiantez

    # Se agregan 2 de las 3 carreras a cada estudiante
    estudiantex.asignaturas = asignaturax
    estudiantex.asignaturas = asignaturay

    estudiantey.asignaturas = asignaturay
    estudiantey.asignaturas = asignaturaz

    estudiantez.asignaturas = asignaturax
    estudiantez.asignaturas = asignaturaz

    # se muestra la lista de estudiantes de una carrera
    lista = carrerax.lista_estudiantes
    for estudiante in lista:
        print(f"{estudiante.nombre_estudiante} saco {estudiante.puntaje}\n")

    # se muestran las 2 asignaturas del estudiantex
    asignaturas_estx = estudiantex.asignaturas
    for asignatura in asignaturas_estx:
        print(f"{asignatura.nombre}\n")

    # se remueve una asignatura
    estudiantex.asignaturas.remove(asignaturax)

    # se vuelven a mostrar las asignaturas
    asignaturas_estx = estudiantex.asignaturas
    for asignatura in asignaturas_estx:
        print(f"{asignatura.nombre}")
