# ejercicio 3

"""
Matricular a un estudiante a una carrera en la universidad y además poder saber
con qué puntaje PTU ingreso.
"""

class Estudiante():
    def __init__(self, carrera, puntaje):
        self._carrera = carrera
        self._puntaje = puntaje

    @property
    def carrera(self):
        return self._carrera

    @property
    def puntaje(self):
        return self._puntaje

    @carrera.setter
    def carrera(self, carrera):
        self._carrera = carrera

    @puntaje.setter
    def puntaje(self, valor):
        self._puntaje = puntaje

class Carrera():
    def __init__(self, nombre_carrera):
        self._nombre_carrera = nombre_carrera

    @property
    def nombre_carrera(self):
        return self._nombre_carrera

    @nombre_carrera.setter
    def nombre_carrera(self, nombre):
        self._nombre_carrera = nombre

if __name__ == "__main__":
    carrerax = Carrera("Bioinformatica")
    roberto = Estudiante(carrerax, 850)
    print(roberto.puntaje)
    print(roberto.carrera.nombre_carrera)
