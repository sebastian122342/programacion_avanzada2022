# ejemplo ex01

#!/usr/bin/env python3
""" Crear una clase para identificar vacunas. Se deberá crear un
constructor que deberá tener como argumentos: el nombre de la vacuna
y el laboratorio que la elaboró, ambos de tipo strings. Se deberá
crear un método en el que se agregarán los efectos secundarios de cada
vacuna (set agrega efecto secundario()) y otro para mostrar los
efectos secundarios de la vacuna. Los efectos secundarios deben ser
almacenados en una lista ([]) y el retorno de los valores deben ser
mostrados linea a linea. Cree a lo menos 3 objetos de tipo Vacuna y
para cada una efectos secundarios."""

import random


lista_efectos = ["Dolor en la zona de vacunación", "Fiebre",
                 "Cefalea", "Mareos", "Mialgia (dolores musculares)",
                 "Artralgia (dolor en las articulaciones)"
                 "Cansancio", "Escalofríos", "Diarrea"]

class Vacuna:
    def __init__(self, nombre, laboratorio):
        self._nombre= nombre
        self._laboratorio = laboratorio
        self._efecto_secundario = []

    @property
    def nombre(self):
        return self._nombre

    @property
    def laboratorio(self):
        return self._laborario

    @property
    def efecto_secundario(self):
        for efecto in self._efecto_secundario:
            print(f"{efecto}")
        print("*"*10)

    @efecto_secundario.setter
    def efecto_secundario(self, efecto):
        if not efecto in self._efecto_secundario:
            self._efecto_secundario.append(efecto)



def efecto_secundarios(vacuna):
        # agrega tres efectos secundarios al azar
    numeros = [random.randint(0, len(lista_efectos) - 1) for x in range(3)]
    for item in numeros:
        vacuna.efecto_secundario = lista_efectos[item]


if __name__ == "__main__":
    vacunax = Vacuna("Vacuna X", "Laboratorio X")
    vacunay = Vacuna("Vacuna Y", "Laboratorio Y")
    vacunaz = Vacuna("Vacuna Z", "Laboratorio Z")
    efecto_secundarios(vacunax)
    efecto_secundarios(vacunay)
    efecto_secundarios(vacunaz)

    print(vacunax.nombre)
    vacunax.efecto_secundario

    print(vacunay.nombre)
    vacunay.efecto_secundario

    print(vacunaz.nombre)
    vacunaz.efecto_secundario
