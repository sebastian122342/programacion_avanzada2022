# ejemplo de los planetas pero con la materia mas actual

"""
Desarrolle una clase que realice la abstracción de un sistema solar, teniendo
en cuenta para cada planeta tienen las siguientes caracterı́sticas: masa,
densidad, diámetro, distancia al sol, un identificador único número y un nombre
de texto. Incluir métodos que muestren la información de los planetas.
"""

import random

class Planet():
    def __init__(self, masa, densidad, diametro, d_sol, id, apodo):
        self._masa = masa
        self._densidad = densidad
        self._diametro = diametro
        self._d_sol = d_sol
        self._id = id
        self._apodo = apodo

    @property
    def masa(self):
        return self._masa

    @property
    def densidad(self):
        return self._densidad

    @property
    def diametro(self):
        return self._diametro

    @property
    def d_sol(self):
        return self._d_sol

    @property
    def id(self):
        return self._id

    @property
    def apodo(self):
        return self._apodo

    @masa.setter
    def masa(self, valor):
        self._masa = valor

    @densidad.setter
    def densidad(self, valor):
        self._densidad = valor

    @diametro.setter
    def diametro(self, valor):
        self._diametro = valor

    @d_sol.setter
    def d_sol(self, valor):
        self._d_sol = valor

    @id.setter
    def id(self, valor):
        self._id = valor

    @apodo.setter
    def apodo(self, string):
        self._apodo = string


def reporte(planeta):
    print(f"REPORTE GENERAL DE {planeta.apodo} id: {planeta.id}")
    print(f"Masa: {planeta.masa}")
    print(f"densidad: {planeta.densidad}")
    print(f"diametro: {planeta.diametro}")
    print(f"distancia al sol: {planeta.d_sol}\n")

if __name__ == "__main__":
    # masa, densidad, diametro, d_sol, id, apodo
    masas = [2400, 12000, 34500, 7500, 4500]
    densidades = [1000, 124500, 245000, 456000, 100000, 9999]
    diametros = [124, 120, 145, 100]
    ds_sol = [10, 1, 140, 100000, 250000]
    ids = [x for x in range(15)]
    apodos = ["jose", "martin", "manuel", "pedro"]


    planetax = Planet(random.choice(masas), random.choice(densidades),
                        random.choice(diametros), random.choice(ds_sol),
                        random.choice(ids), random.choice(apodos))

    planetay = Planet(random.choice(masas), random.choice(densidades),
                        random.choice(diametros), random.choice(ds_sol),
                        random.choice(ids), random.choice(apodos))

    planetaz = Planet(random.choice(masas), random.choice(densidades),
                        random.choice(diametros), random.choice(ds_sol),
                        random.choice(ids), random.choice(apodos))
    reporte(planetax)
    reporte(planetay)
    reporte(planetaz)
