# Main

"""
Proyecto 1 programacion avanzada

Se hace una simulacion de la propagacion de una enfermedad, en una poblacion
determinada.

Esta simulacion se hace apegandose al modelo SIR.
"""

# Se importan los modulos necesarios
from disease import Disease
from person import Person
from community import Community
from simulator import Simulator

# Se crea enfermedad, comunidad y se ejecuta la simulacion
covid = Disease(0.8, 14)
talca = Community(1000, 5, covid, 0.5)
talca.create_people()
talca.create_contacts()
sim = Simulator(talca)
sim.run(100)
