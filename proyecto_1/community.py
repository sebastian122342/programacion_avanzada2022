# Comunidad

# Se importan los modulos necesarios
import random as r
import time as t
from person import Person

r.seed(t.time())

class Community():
    """ Clase dedicada al objeto tipo Community """
    def __init__(self, people, average_physical_connection, disease,
                physical_connection_probability):
        # Se establece la comunidad
        self.num_people = people
        self.average_physical_connection = average_physical_connection
        self.disease = disease
        self.physical_connection_probability =  physical_connection_probability
        # Lista donde se conservaran los objetos Person
        self.hab_list = []


    def create_people(self):
        """ Crea personas, asignando numero de familia """
        # Cada familia tiene de 2 a 7 miembros
        family = 1
        family_length = r.randint(2, 7)
        members = 0
        for x in range(self.num_people):
            id = x + 1
            if members == family_length:
                # Una vez se llena de miembros, se cambia de familia
                family_length = r.randint(2, 7)
                family += 1
                members = 0
            else:
                members += 1

            # Se crea una persona, determinando su cantidad de contactos
            average = self.average_physical_connection
            contact_length = r.randint(average-2, average+2)
            human_being = Person(self, id, family, self.disease)
            human_being.len_contacts = contact_length
            self.hab_list.append(human_being)


    def random_person(self):
        """ Se selecciona una persona aleatoria de la lista de habitantes """
        return r.choice(self.hab_list)


    def create_contacts(self):
        """ Se determina la lista de contactos estrechos de cada persona """
        for person in self.hab_list:
            contact_length = person.len_contacts - len(person.contacts)
            if contact_length <= 0:
                continue

            sample = r.sample(self.hab_list, contact_length)
            while person in sample:
                sample = r.sample(self.hab_list, contact_length)

            for contact in sample:
                if not len(contact.contacts) == contact.len_contacts:
                    if not person in contact.contacts:
                        person.contacts.append(contact)
                        contact.contacts.append(person)


    def report(self):
        """ Se hace un reporte de enfermos, recuperados y susceptibles """
        healthy = 0
        done = 0
        ill = 0
        for person in self.hab_list:
            if not person.done and not person.healthy:
                ill += 1
            elif not person.done and person.healthy:
                healthy += 1
            else:
                done += 1
        print(f"{healthy} susceptibles, {done} recuperados y {ill} infectados")
