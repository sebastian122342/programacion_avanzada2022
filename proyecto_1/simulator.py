# Simulador

# Se importan los modulos necesarios
import random as r
from disease import Disease
from community import Community

# Decorador para mostrar la informacion
def print_decorator(func):
    def wrap(text):
        print("="*80)
        func(text)
        print("")
    return wrap
@print_decorator
def print_w_format(text):
    print(text)


class Simulator():
    """ Clase dedicada el objeto simulador """
    def __init__(self, community):
        # Se establece el simulador
        self.community = community
        # Enfermedad que afecta a la comunidad
        self.disease = self.community.disease


    def run(self, steps):
        """ Ejecucion de la simulacion de contagios """

        # Se escoge una persona al azar que sera el paciente 0
        patient0 = self.community.random_person()
        patient0.set_infected()

        # Comienza la simulacion por pasos
        for step in range(steps):
            print_w_format(f"step: {step+1}")
            for person in self.community.hab_list:
                # Si la persona ya esta lista no pasa nada
                if person.done:
                    continue
                # Se revisa el estado de todos los enfermos
                if not person.healthy:
                    person.current_step_disease += 1
                    # Si la persona ya cumplio sus dias de enfermedad, se acaba
                    if person.current_step_disease == person.total_step_disease:
                        person.set_recovered()
                        continue
                    # Si la persona aun sigue enferma, puede contagiar
                    meet = person.encounter()
                    if meet:
                        if meet.healthy and not meet.done:
                            if self.disease.prob_infection*100 >= r.randint(0,100):
                                meet.set_infected()

            # Reporte de susceptibles, infectados y recuperados por cada step
            self.community.report()
