# Persona

# Se importa el modulo necesario
import random as r

class Person():
    """ Clase dedicada al objeto tipo Person """
    def __init__(self, community, id, family, disease):
        # Se establece a la persona y sus atributos
        self.community = community
        self._id = id
        self.family = family
        self.disease = disease
        self.done = False
        self.healthy = True
        self.total_step_disease = None
        self.current_step_disease = None
        self.len_contacts = None
        self.contacts = []

    # Getter y Setter para el id de la persona
    @property
    def id(self):
        return self._id
    @id.setter
    def id(self, id):
        self._id = id


    def set_infected(self):
        """ Metodo creado para enfermar a una persona """
        self.healthy = False
        # Se determina cuanto durara la enfermedad
        self.total_step_disease = self.disease.average_steps + r.randint(-2, 2)
        self.current_step_disease = 0


    def set_recovered(self):
        """ Se establece que la persona esta lista: muerta o recuperada """
        self.done = True


    def encounter(self):
        """ Encuentro fisico con una persona de la lista de contactos """
        prob = self.community.physical_connection_probability
        if prob * 100 >= r.randint(0,100):
            return r.choice(self.contacts)
        return None

    # Metodos que fueron utilizados para ordenar a las personas por id
    # al momento de crear lo grupos de contactos
    def __gt__(self, other):
        if self._id > other._id:
            return True
        else:
            return False
    def __lt__(self, other):
        if self._id < other._id:
            return True
        else:
            return False
