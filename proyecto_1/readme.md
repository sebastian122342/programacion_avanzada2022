# Proyecto 1 - Programación Avanzada
Se crea un programa en python que permite hacer una simulación de la
propagación de una enfermedad infecciosa en una comunidad.

Esta simulación se apega a las premisas del modelo SIR.

## Integrantes:
* Sebastián Bustamante
* Diego Núñez
* Magdalena Lolas

## Modelo SIR
En el modelo SIR se tiene una comunidad "cerrada", esto es, la cantidad
de individuos que tendrá la comunidad de la simulación será fija durante todo
momento. No se "agregan" individuos de comunidades externas, ni nade "escapa".

El nombre del modelo SIR hace referencia a los 3 grupos que se manejan en la
comunidad. Susceptibles, Infectados y Recuperados. El modelo considera
a los individuos fallecidos y recuperados como parte del mismo grupo, alguien
recuperado no se puede volver a enfermar, por lo que el flujo de individuos de
grupo a grupo irá siempre en un solo sentido: S --> I --> R.

Las premisas del modelo SIR son las siguientes:
* La cantidad de Infectados a medida que la simulación avanza aumentará hasta
llegar a un único peak, luego disminuirá tendiendo a 0.
* Los muertos y recuperados se agregan al mismo grupo.
* Un individuo recuperado no se puede volver a enfermar.
* Un individuo recuperado no puede contagiar a otros.
* Todos los individuos inician siendo igual de susceptibles.
* La cantidad total de personas en la comunidad es constante.
* Se maneja solamente una enfermedad.
* La población es finita.

## Los objetos
El programa presenta una solución orientada a objetos.
Estos objetos interactúan entre sí para llevar a cabo la simulación.

### Community
Representa la comunidad en la cual se hará la simulación.
Esta contiene una cierta cantidad de objetos Person, también se especifica
la enfermedad y otros atributos relevantes para la simulación.

### Person
Corresponde a una persona que pertenece a una comunidad, tiene atributos
para poder verificar su estado de salud.

Un punto importante respecto a las personas, es que cada una de ellas
tiene un atributo contacts que corresponde a una lista de objetos tipo persona,
representa los contactos a los cuales la persona podría contagiar en caso de
enfermedad.

La familia de una persona, para el caso de la simulación, simplemente se trata
como un número.

### Disease
Corresponde a la enfermedad con la cual se trabajará en la simulación.
Tiene una cierta probabilidad de infectar y un promedio de pasos en los cuales
la persona se recuperará/morirá.

### Simulator
Corresponde a la clase en la cual se ejecuta la simulación en sí, con los
diferentes objetos previamente mencionados.

Se determina la comunidad y enfermedad con la cual se trabajará, y el método
run() donde por una cierta cantidad de steps se ejecutará la simulación.
Antes de entrar al ciclo se determina un paciente 0, que es una persona al
azar a la cual se infecta, para que la enfermedad se empiece a propagar.

### main
Corresponde a la clase en la cual se inicializa todo lo necesario para la
simulación y esta es llevada a cabo.
