# vaccine

class Vaccine():
    def __init__(self, immunity, doses, steps, vax_id):
        self.immunity = immunity
        self.num_doses = doses
        self.steps = steps
        self.vax_id = vax_id

    def apply_immunity(self, person):
        person.immunity = self.immunity

    @classmethod
    def new_vaccine1(cls):
        return cls(0.25, 2, 3, 1)

    @classmethod
    def new_vaccine2(cls):
        return cls(0.5, 2, 6, 2)

    @classmethod
    def new_vaccine3(cls):
        return cls(1, 1, 0, 3)

    @classmethod
    def no_vaccine(cls):
        return cls(0, 0, 0, 0)
