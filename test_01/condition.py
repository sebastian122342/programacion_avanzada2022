# Condition

class Condition():
    def __init__(self, name, gravity):
        self.name = name
        self.gravity = gravity

    @classmethod
    def obesity(cls):
        return cls("Obesidad", 0.5)

    @classmethod
    def malnutrition(cls):
        return cls("Desnutricion", 0.2)

    @classmethod
    def no_condition(cls):
        return cls("Sin condicion", 0)
