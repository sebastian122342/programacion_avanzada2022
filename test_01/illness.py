# illnes

class Illness():
    def __init__(self, name, gravity):
        self.name = name
        self.gravity = gravity

    @classmethod
    def asthma(cls):
        return cls("Asma", 0.7)

    @classmethod
    def cereb_disease(cls):
        return cls("Enfermedad cerebrovascular", 0.6)

    @classmethod
    def cystic_fibrosis(cls):
        return cls("Fibrosis quistica", 0.9)

    @classmethod
    def hypertension(cls):
        return cls("Hipertension", 0.4)

    @classmethod
    def no_illness(cls):
        return cls("Sin enfermedad", 0)
