# ejercicio 2

"""
Un punto en el tiempo deberia tener un año, mes, dia, hora, segundo.
(milisegundo?).
reglas de negocio para que si el mes de 02 no hacer que el dia sea mayor a 28
o algo asi dependiendo de si el año es bisiesto o no.
"""

class PointInTime():
    def __init__(self, day, month, year, second, minute, hour):
        self._day = day
        self._month = month
        self._year = year
        self._second = second
        self._minute = minute
        self._hour = hour

    @property
    def day(self):
        return self._day

    @property
    def month(self):
        return self._month

    # MESES Y DIAS ES MAS DIFICIL DE AYER PORQUE UNO CONDICIONA AL OTRO

    @property
    def year(self):
        return self._year

    @year.setter
    def year(self, value):
        if value >= 0:
            self._year = value
        else:
            print("valor de año no valido")

    @property
    def second(self):
        return self._second

    @second.setter
    def second(self, value):
        if 0 <= value <= 59:
            self._second = value
        else:
            print("Valor de segundo no valido")

    @property
    def minute(self):
        return self._minute

    @minute.setter
    def minute(self, value):
        if 0 <= value <= 59:
            self._minute = value
        else:
            print("Minuto no valido")


    @property
    def hour(self):
        return self._hour

    @hour.setter
    def hour(self, value):
        if 1 <= value <= 24:
            self._hour = value
        else:
            print("Hora no valida")




def main():
    pass

if __name__ == "__main__":
    main()
