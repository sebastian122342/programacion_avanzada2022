# ejercicio 1 guia 5

"""
Hacer el juego de dados pero con objeton en python
"""

import random

class Dado():
    """
    Clase para el dado. Este se puede tirar y obtener un valor del 1 al 6
    """
    def __init__(self):
        self._valor = None

    @property
    def valor(self):
        return self._valor

    @valor.setter
    def valor(self, num):
        self._valor = num


class YahtzeeTable():
    """
    Clase para la mesa. Esta recibe los dados y registra puntajes.
    """
    def __init__(self):
        # se definen atributos de la upper y lower section
        self._upper_section = []
        self._lower_section = []

    # acciones que permiten agregar los puntos asociados a cada tiro
    @property
    def upper_section(self):
        return self._upper_section

    @upper_section.setter
    def upper_section(self, score):
        self._upper_section.append(score)

    @property
    def lower_section(self):
        return self._lower_section

    @lower_section.setter
    def lower_section(self, score):
        self._lower_section.append(score)


def ask_number():
    # Pide un numero. Usada mas adelante en menu()
    number = input("Ingrese un numero: ")
    while not number.isdigit():
        number = input("Ingrese un numero: ")
    return int(number)

def menu():
    # Genera un menu con opciones. La ultima sera "Salir"
    lineas = ["Tirar dados"]
    print("\n\t\tMenu\n")
    for x in range(len(lineas)):
        print(f"<{x+1}>\t{lineas[x]}")
    print(f"<{len(lineas)+1}>\tSalir")
    while True:
        print("\n¿Que quiere hacer?")
        opcion = ask_number()
        if 1 <= opcion <= len(lineas)+1:
            return opcion  # Se retorna la opcion elegida
        else:
            print("Esa opcion no es valida")


def calculo_upper(dados):
    # se calcula el puntaje que debera ser agregado a la upper section
    valores = []
    puntaje = 0
    for dado in dados:
        valores.append(dado.valor)
    for num in range(1, 7):
        if valores.count(num) > 1:
            puntaje += num * valores.count(num)
    return puntaje

def calculo_lower(dados):
    valores = []
    for dado in dados:
        valores.append(dado.valor)
    # 3 de una clase
    for num in range(1, 7):
        if valores.count(num) == 3:
            return sum(valores)
    # 4 de una clase
    for num in range(1, 7):
        if valores.count(num) == 4:
            return sum(valores)
    # Full house. 3 dados de una clase y 2 de otra.
    for num in range(1, 7):
        if valores.count(num) == 3:
            for num2 in range(1, 7):
                if valores.count(num2) == 2:
                    return 25
    # small straight. 4 dados consecutivos
    pos1 = [1, 2, 3, 4]
    pos2 = [2, 3, 4, 5]
    pos3 = [3, 4, 5, 6]
    items = set(valores)
    if set(pos1).issubset(items) or set(pos2).issubset(items) or set(pos3).issubset(items):
        return 30

    # large straight. 5 dados consecutivos
    pos1 = [1, 2, 3, 4, 5]
    pos2 = [2, 3, 4, 5, 6]
    if set(pos1).issubset(items) or set(pos2).issubset(items):
        return 40

    # Yahtzee. 5 dados iguales de la misma clase
    for num in range(1, 7):
        if valores.count(num) == 5:
            return 50

    # chance. cuando no queda de otra, solo se suman todos los valores.
    return sum(valores)

def main():
    # Se crea una lista con 5 dados, aun sin valor.
    dados = []
    for x in range(0, 5):
        dados.append(Dado())
    tabla = YahtzeeTable()
    partidas = 0
    while partidas < 8:
        opcion = menu()
        if opcion == 1:
            retrys = 0
            while retrys < 3:
                total = ""
                for dado in dados:
                    dado.valor = random.randint(1, 6)
                    total += f"{dado.valor} - "
                total = total.rstrip("- ")
                print(f"Lanzamiento\n{total}")

                #pedir confirmacion del tiro
                print(f"\nConfirmar lanzamiento? Quedan {3 - retrys} intentos")
                print("<1> Si <2> No")
                num = ask_number()
                while not 1 <= num <= 2:
                    num = ask_number()
                if num == 1:
                    break
                else:
                    retrys += 1

            total = ""
            for dado in dados:
                total += f"{dado.valor} - "
            total = total.rstrip("- ")
            print(f"\nLanzamiento oficial\n{total}")

            # meter la lista de dados en la tabla
            punto_upper = calculo_upper(dados)
            punto_lower = calculo_lower(dados)
            tabla.upper_section = punto_upper
            tabla.lower_section = punto_lower
            partidas += 1

        elif opcion == 2:
            exit()

    print("Se han acabado las jugadas!")
    # MOSTRAR RESUMENES DE COSAS
    puntajes_upper = tabla.upper_section
    puntajes_lower = tabla.lower_section
    if sum(puntajes_upper) > 63:
        puntaje_upper_final = sum(puntajes_upper) + 35
    print(f"\nPUNTAJE UPPER: {puntaje_upper_final}")
    print(f"PUNTAJE LOWER: {sum(puntajes_lower)}")
    print(f"PUNTOS TOTALES: {sum(puntajes_lower) + puntaje_upper_final}")

if __name__ == "__main__":
    main()
