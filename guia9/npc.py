# Clase Npc madre de wizard y warrior

from plano import Plano

class Luchador(Plano):
    def __init__(self):
        # A cada luchador se le define una vida base de 100 puntos
        self._health = 100

    # Setter y getter de la salud
    @property
    def health(self):
        return self._health

    @health.setter
    def health(self, value):
        if isinstance(value, int):
            self._health = value
        else:
            print("tipo de dato no valido")

    def isVulnerable(self):
        # El metodo isVulnerable retorna True por defecto
        return False

    # No se asigna comportamiento pues sera reescrito en las dos clases hijas
    def damagePoints(self, obj):
        pass
