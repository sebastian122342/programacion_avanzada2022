# Plano de la clase Npc

from abc import abstractmethod
from abc import ABCMeta

# Se deben tener los metodos isVulnerable y damagePoints
class Plano(metaclass = ABCMeta):
    @abstractmethod
    def isVulnerable(self):
        pass

    @abstractmethod
    def damagePoints(self, obj):
        pass
