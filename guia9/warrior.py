# Clase warrior hija de Npc

from npc import Luchador

class Warrior(Luchador):
    def __init__(self):
        super().__init__()

    def toString(self):
        # Se define toString que deja un mensaje
        print("Hola soy un warrior")

    def damagePoints(self, obj):
        # Cuando el warrior hace daño, se verifica si su objetivo es vulnerable
        if obj.isVulnerable():
            dmg = 10
        else:
            dmg = 6
        print(dmg)
        obj.health = obj.health - dmg
