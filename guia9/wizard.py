# Clase wizard hija de Npc

from npc import Luchador

class Wizard(Luchador):
    def __init__(self):
        super().__init__()
        self._spell = False

    # Setter y getter del hechizo
    @property
    def spell(self):
        return self._spell

    @spell.setter
    def spell(self, value):
        if isinstance(value, bool):
            self._spell = value
        else:
            print("tipo de dato no valido")

    def prepareSpell(self):
        # Cuando se prepara un hechizo, el boolean spell se vuelve verdadero
        self.spell = True

    def isVulnerable(self):
        # Si es vulnerable o no depende de si hay o no hay hechizo
        return not self.spell

    def damagePoints(self, obj):
        # Cuando el wizard hace daño, se verifica si tiene hechizo preparado
        if self.spell:
            # El hechizo se gasta entonces ya no tiene uno preparado
            self.spell = False
            dmg = 12
        else:
            dmg = 3
        print(dmg)
        obj.health = obj.health - dmg
