
from wizard import Wizard
from warrior import Warrior
import random

"""
Guia 9 por Sebastián Bustamante Villarreal

Para el desarrollo de la guía se estimó conveniente no utilizar
sobrecarga en ningún método, solamente se redefinieron métodos en una clase
según fuera necesario.

La clase Npc(que corresponde a la clase madre de warrior y wizard) tiene
los metodos isVulnerable y damagePoints. Esta clase esta creada a partir de la
clase abstracta Plano.

El metodo isVulnerable retorna False como valor, esto es reescrito en wizard,
haciendo que retorne lo contrario al estado de su echizo (si tiene, retorna
False, si no tiene, retorna True)
Como warrior nunca es vulnerable, no se reescribe.

Al metodo damagePoints no se le asigna comportamiento en la clase npc ya que
este funciona de forma diferente dependiendo de si quien ataca es un warrior o
un wizard. Se reescribe para ambas clases.

En la clase main Wizards_and_Warriors se hacen unas mini_simulaciones y un
versus wizard/warrior para mostrar el comportamiento de las clases

"""

class Wizards_and_Warriors():

    def mini_simulacion1(self):
        # El mago le hace daño a un enemigo sin preparar hechizo
        wizard = Wizard()
        warrior = Warrior()

        print("-"*50)
        print("Caso 1. El mago hace daño sin preparar hechizo")
        wizard.damagePoints(warrior)
        print("-"*50)

    def mini_simulacion2(self):
        # El mago le hace daño a un enemigo luego de preparar hechizo
        wizard = Wizard()
        warrior = Warrior()
        wizard.prepareSpell()

        print("-"*50)
        print("Caso 2. El mago hace daño luego de preparar hechizo")
        wizard.damagePoints(warrior)

    def mini_simulacion3(self):
        # El guerrero le hace daño a un enemigo vulnerable
        wizard = Wizard()
        warrior = Warrior()
        warrior.toString()

        print("-"*50)
        print("Caso 3. El guerrero hace daño a un vulnerable")
        warrior.damagePoints(wizard)
        print("-"*50)

    def mini_simulacion4(self):
        # El guerrero le hace daño a otro enemigo no vulnerable
        warrior = Warrior()
        wizard = Wizard()
        wizard.prepareSpell()

        print("-"*50)
        print("Caso 4. El guerrero hace daño a un no vulnerable")
        warrior.damagePoints(wizard)
        print("-"*50)

    def versus(self):
        print("-"*50)
        print("VERSUS")
        wizard = Wizard()
        warrior = Warrior()
        jugador = random.choice((wizard, warrior))
        while True:
            if jugador == wizard:
                # Ataca o prepara hechizo
                jugada = random.randint(0, 1)
                if jugada == 0:
                    print("Wizard prepara un hechizo")
                    jugador.prepareSpell()
                else:
                    print("Wizard ataca")
                    jugador.damagePoints(warrior)
                jugador = warrior
            else:
                # Solo puede atacar
                print("Warrior ataca")
                jugador.damagePoints(wizard)
                jugador = wizard
            # Por cada turno se ve si hay ganadores
            if wizard.health <= 0:
                ganador = "warrior"
                break
            if warrior.health <= 0:
                ganador = "wizard"
                break
        # Se comunica el ganador
        print(f"El ganador es {ganador}")
        print("-"*50)

if __name__ == "__main__":
    # Se crea la clase main y se hacen 4 "mini simulaciones" para mostrar
    # diferentes formas de interaccion entre las clases
    # tambien un versus entre warrior y wizard, hasta que uno muera

    tabla = Wizards_and_Warriors()
    tabla.mini_simulacion1()
    tabla.mini_simulacion2()
    tabla.mini_simulacion3()
    tabla.mini_simulacion4()
    tabla.versus()
