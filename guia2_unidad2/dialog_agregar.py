# Dialogo de agregar informacion

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class DialogAdd(Gtk.Dialog):
    """ Ventana de dialogo para poder agregar informacion """
    def __init__(self):
        # Se establece la ventana
        super().__init__(title="Agregar informacion")
        self.set_modal(True)
        self.add_buttons(Gtk.STOCK_OK, Gtk.ResponseType.OK,
                        Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)
        self.set_default_size(600, 600)

        # Se establece el area de accion, se le agrega una Grid
        self.box = self.get_content_area()
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.box.pack_start(self.grid, True, True, 1)

        # Label para el ID de la pelicula
        self.lbl_id = Gtk.Label(label="ID:")
        self.grid.attach(self.lbl_id, 0, 0, 1, 1)

        # Entrada para el ID
        self.entry_id = Gtk.Entry()
        self.entry_id.set_placeholder_text("Escribir ID")
        self.grid.attach(self.entry_id, 1, 0, 1, 1)

        # Label para el nombre de la pelicula
        self.lbl_name = Gtk.Label(label="Nombre:")
        self.grid.attach(self.lbl_name, 0, 1, 1, 1)

        # Entrada para el nombre
        self.entry_name = Gtk.Entry()
        self.entry_name.set_placeholder_text("Escribir nombre")
        self.grid.attach(self.entry_name, 1, 1, 1, 1)

        # Label para el rating
        self.lbl_rating = Gtk.Label(label="Rating:")
        self.grid.attach(self.lbl_rating, 0, 2, 1, 1)

        # ComboBoxText para el rating, notas del 1 al 10
        self.combo_rating = Gtk.ComboBoxText()
        for x in range(1, 11):
            self.combo_rating.append_text(str(x))
        self.combo_rating.set_active(0)
        self.grid.attach(self.combo_rating, 1, 2, 1, 1)

        #Scrolled window para la textview
        self.scroll = Gtk.ScrolledWindow()
        self.grid.attach(self.scroll, 0, 3, 4, 4)

        # Texview - Tiene un Wrap que corta palabras y letras
        self.textview = Gtk.TextView()
        self.textview.set_wrap_mode(Gtk.WrapMode(Gtk.WrapMode.WORD_CHAR))
        self.buffer = self.textview.get_buffer()
        self.buffer.set_text("Escriba aqui la reseña")
        self.scroll.add(self.textview)

        self.show_all()
