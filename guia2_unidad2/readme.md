# Guia 2 Unidad 2 - Programacion Avanzada

## Autores
* Sebastián Bustamante Villarreal
* Bastián Morales Aravena

## Ventana Principal
La clase MainWindow hereda de Gtk.Window y es el objeto principal.
Posee un objeto Gtk.HeaderBar y un Gtk.Grid que contienen los demás objetos.
La HeaderBar contiene los botones de about y FileChooser.
La Grid contiene las las ScrolledWindow y los botones de agregar y eliminar
entradas.
Cada ScrolledWindow contiene el objeto TreeView y la Label para mostrar la
review.
Los widgets asociados a la visualizacion de informacion en la ventana principal
solamente se muestran una vez se escoge un archivo csv en el FileChooser.

![MainWindow](./pics/main_window.png)

### Cargar del csv

El metodo cargar_del_csv genera un dataframe a partir de un archivo csv usando
pandas, con read_csv. El dataframe ordena la informacion contenida en el csv y
permite que sea de facil acceso, similar a un diccionario.

El metodo permite traspasar la informacion del archivo csv al TreeView, y se
puede utilizar tanto para la primera vez que se abre el archivo como para todas
las veces en las que se decide agregar o eliminar informacion, para refrescar la
pantalla.

    # Si ya hay data en el treeview, se quitan las columnas
    if self.tree.get_columns():
        for column in self.tree.get_columns():
            self.tree.remove_column(column)

Luego se necesita definir un modelo para el TreeView el cual sera una ListStore.
En la ListStore se debe especificar cuantas columnas se tendran y de que tipo,
para este casos son todas string.

    # Se construye el modelo
    largo_columnas = len(data.columns)
    modelo = Gtk.ListStore(*(largo_columnas * [str]))
    self.tree.set_model(model=modelo)

Despues de definir el modelo se establecen las columnas en el TreeView. Los
nombres de las columnas del TreeView seran las mismas que las del dataframe.

    # Se colocan las columnas
    cell = Gtk.CellRendererText()
    for item in range(len(data.columns)):
        column = Gtk.TreeViewColumn(data.columns[item], cell, text=item)
        self.tree.append_column(column)
        column.set_sort_column_id(item)
        # Se oculta la columna que no sera mostrada
        if item == 3:
            column.set_visible(False)

Para el caso de la aplicacion desarrollada, se oculta la columna 3 ya que
la informacion relacionada a esta sera mostrada en un label al costado derecho
de la ventana principal.

Finalmente se termina de llenar el TreeView con la informacion, accediendo a
los valores del dataframe

    # Se rellena el tree con la data
    for item in data.values:
        line = [str(x) for x in item]
        modelo.append(line)

## Dialogo Agregar
La clase DialogAdd hereda de Gtk.Dialog y contiene los widgets necesarios
para poder agregar una entrada en el archivo csv. La validacion de que la
informacion que contiene la ventana de dialogo sirve para poder ser agregada
al archivo se hace en la clase principal.

Se pide que la ID sea numerica, se ingrese un nombre para la pelicula (no ""),
una nota (escogida mediante una Gtk.ComboBoxText) y una review (no "").

    # Chequeo de errores
      principal = "Error"
      if not id.isnumeric():
          secundaria = "Se debe ingresar una ID numerica"
          error = True
      if name == "":
          secundaria = "Debe ingresar un nombre"
          error = True
      if review == "":
          secundaria = "Debe escribir una review"
          error = True
      if error:
          on_error_clicked(dialogo, principal, secundaria)
          dialogo.destroy()
          return

![DialogAdd](./pics/dialog_add.png)

## Filechooser
La ventana Filechooser hereda de Gtk.FileChooserDialog y se encarga de
hacer que el usuario escoja un archivo para visualizar en el treeview.
La validacion de que el archivo escogido sea valido se hace en la clase
principal. Si es que hay errores se comunica y se cierra la ventana para poder
volver a intentar.

Validacion:

      self.archivo = filechooser.get_filename()
      root, extension = os.path.splitext(self.archivo)
      if extension == ".csv":
          self.cargar_del_csv(self.archivo)
          self.tree.show()
          self.btn_add.show()
          self.btn_delete.show()
          self.btn_filechooser.hide()
      else:
          on_error_clicked(self, "Error", "Deber ser archivo csv")

![Filechooser](./pics/filechooser.png)

## Ventana About
La clase AboutDialog hereda de Gtk.AboutDialog y utiliza los metodos propios
de la clase padre para poder definir informacion relevante de manera sencilla.
Se definen nombre de los autores, nombre del programa, version, icono, etc.

    super().__init__(title="Dialogo About")
    self.set_modal(True)
    autores = ["Sebastián Bustamante", "Bastián Morales"]
    self.add_credit_section("Autores", autores)
    self.set_comments("Segunda Guia Unidad 2, Programacion Avanzada")
    self.set_logo_icon_name("input-gaming")
    self.set_program_name("IMDB pirata")
    self.set_version("1.0")

## Mensajes
El archivo Messages.py contiene funciones que permiten crear ventanas de dialogo
de mensaje (Gtk.MessageDialog) de distintos tipos para poder comunicar
informacion al usuario de manera sencilla. Para este caso se usaron mensajes
de error y de pregunta.

![on_error_clicked](./pics/on_error_clicked.png) ![on_question_clicked](./pics/on_question_clicked.png)
