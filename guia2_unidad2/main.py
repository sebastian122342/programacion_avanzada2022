"""
Guia 2 Unidad 2 - Programacion Avanzada

Por Sebastián Bustamante y Bastián Morales

Se crea una aplicacion sencilla en la cual, por medio de un FileChooser, se
puede cargar la informacion de un archivo csv para poder visualizarla, agregar
o eliminar entradas. Para agregar entradas se da uso del widget TextView.

La aplicacion esta principalmente orientada a que el archivo cargado sea
"file_data.csv" que ya se encuentra en la carpeta de la guia. Corresponde a
la informacion asociada a peliculas.
"""

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gio
from gi.repository import GdkPixbuf

from about_dialog import AboutDialog

from messages import on_error_clicked
from messages import on_question_clicked

from filechooser import FileChooser

from dialog_agregar import DialogAdd

import pandas as pd

import os


def generar_linea(lista):
    """ Toma una lista y genera una linea para el archivo csv """
    # la lista tiene el orden id-name-rating-review
    # Si algun elemento tiene "," en su interior, se agregan comillas
    for x in range(len(lista)):
        lista[x] = f'"{lista[x]}"'
    linea = ",".join(lista) + "\n"
    return linea


def agregar_data(linea, archivo):
    """ Agrega la linea deseada a un archivo """
    with open(archivo, "a") as file:
        file.write(linea)


class MainWindow(Gtk.Window):
    def __init__(self):
        # Se establece la ventana
        super().__init__()
        self.set_default_size(600, 600)
        self.set_border_width(20)
        self.connect("destroy", Gtk.main_quit)

        # Se crea la headerbar
        self.hb = Gtk.HeaderBar()
        self.hb.set_title("Reseña de peliculas")
        self.hb.set_subtitle("Guia 2 Unidad 2 - Programacion Avanzada")
        self.hb.set_show_close_button(True)
        self.set_titlebar(self.hb)

        # Boton Ventana About
        self.btn_about = Gtk.Button()
        icon = Gio.ThemedIcon(name="help-about")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_about.add(image)
        self.btn_about.connect("clicked", self.on_btn_about)
        self.hb.pack_end(self.btn_about)

        # Boton Filechooser
        self.btn_filechooser = Gtk.Button()
        icon = Gio.ThemedIcon(name="media-floppy")
        image = Gtk.Image.new_from_gicon(icon, Gtk.IconSize.BUTTON)
        self.btn_filechooser.add(image)
        self.btn_filechooser.connect("clicked", self.on_btn_filechooser)
        self.hb.pack_start(self.btn_filechooser)

        # Grid que contiene las scrolled windows
        self.grid = Gtk.Grid()
        self.grid.set_row_spacing(20)
        self.grid.set_column_spacing(20)
        self.add(self.grid)

        # Scrolled window para el treeview
        self.scroll1 = Gtk.ScrolledWindow()
        self.grid.attach(self.scroll1, 0, 0, 15, 30)

        # Scrolled window para la label de review
        self.scroll2 = Gtk.ScrolledWindow()
        self.grid.attach_next_to(self.scroll2, self.scroll1,
                                Gtk.PositionType.RIGHT, 10, 10)

        # Treeview
        self.tree = Gtk.TreeView()
        self.tree.connect("row-activated", self.tree_row_activated)
        self.tree.set_activate_on_single_click(True)
        self.scroll1.add(self.tree)

        # Label para la review
        self.lbl_review = Gtk.Label()
        self.lbl_review.set_line_wrap(True)
        self.lbl_review.set_halign(Gtk.Align.START)
        self.lbl_review.set_valign(Gtk.Align.START)
        self.scroll2.add(self.lbl_review)

        # Boton de añadir reseña
        self.btn_add = Gtk.Button(label="añadir")
        self.btn_add.connect("clicked", self.on_btn_add)
        self.grid.attach_next_to(self.btn_add, self.scroll2,
                                Gtk.PositionType.BOTTOM, 1, 1)

        # Boton de eliminar reseña
        self.btn_delete = Gtk.Button(label="eliminar")
        self.btn_delete.connect("clicked", self.on_btn_delete)
        self.grid.attach_next_to(self.btn_delete, self.btn_add,
                                Gtk.PositionType.RIGHT, 1, 1)

        # Se muestra todo a excepcion de los widgets relacionados a mostrar
        # la informacion del archivo escogido por motivos esteticos
        self.show_all()
        self.lbl_review.hide()
        self.tree.hide()
        self.btn_add.hide()
        self.btn_delete.hide()


    def on_btn_filechooser(self, btn):
        """ Se abre el filechooser para escoger un archivo """
        filechooser = FileChooser()
        resp = filechooser.run()
        if resp == Gtk.ResponseType.OK:
            # Una vez se escoge archivo se muestran los demas widgets
            # a excepcion del label de review
            self.archivo = filechooser.get_filename()
            root, extension = os.path.splitext(self.archivo)
            if extension == ".csv":
                self.cargar_del_csv(self.archivo)
                self.tree.show()
                self.btn_add.show()
                self.btn_delete.show()
            else:
                on_error_clicked(self, "Error", "Deber ser archivo csv")
        filechooser.destroy()


    def cargar_del_csv(self, archivo, box=None):
        """ Se carga la info del csv a un treeview """
        # Se lee el archivo y se arma un dataframe
        self.data = pd.read_csv(archivo)

        # Si ya hay data en el treeview, se quitan las columnas
        if self.tree.get_columns():
            for column in self.tree.get_columns():
                self.tree.remove_column(column)

        # Se construye el modelo
        largo_columnas = len(self.data.columns)
        modelo = Gtk.ListStore(*(largo_columnas * [str]))
        self.tree.set_model(model=modelo)

        # Se colocan las columnas
        cell = Gtk.CellRendererText()
        for item in range(len(self.data.columns)):
            column = Gtk.TreeViewColumn(self.data.columns[item], cell, text=item)
            self.tree.append_column(column)
            column.set_sort_column_id(item)
            # Se oculta la columna que no sera mostrada
            if item == 3:
                column.set_visible(False)

        # Se rellena el tree con la data
        for item in self.data.values:
            line = [str(x) for x in item]
            modelo.append(line)


    def tree_row_activated(self, model, path, iter):
        """ Evento cuando se activa una fila del TreeView """
        model, it = self.tree.get_selection().get_selected()
        if model is None:
            return False
        # Se muestra el label de la review, con la review correspondiente.
        self.lbl_review.show()
        self.lbl_review.set_label(model.get_value(it, 3))


    def on_btn_add(self, btn):
        """ Se crea una ventana de dialogo para agregar la nueva info """
        ids = []
        for id in self.data.id:
            ids.append(str(id))
        error = False
        dialogo = DialogAdd()
        resp = dialogo.run()
        if resp == Gtk.ResponseType.OK:
            id = dialogo.entry_id.get_text().strip(" ")
            name = dialogo.entry_name.get_text().strip(" ")
            rate = dialogo.combo_rating.get_active_text()
            review = dialogo.buffer.props.text.strip(" ")

            # Chequeo de errores
            principal = "Error"
            if not id.isnumeric():
                secundaria = "Se debe ingresar una ID numerica"
                error = True
            elif id in ids:
                secundaria = "La ID debe ser unica"
                error = True
            if name == "":
                secundaria = "Debe ingresar un nombre"
                error = True
            if review == "":
                secundaria = "Debe escribir una review"
                error = True
            if error:
                on_error_clicked(dialogo, principal, secundaria)
                dialogo.destroy()
                return

            # Si no hay errores, se crea una lista con la informacion,
            # se genera una linea y se agrega al csv
            lista = [id, name, rate, review]
            linea = generar_linea(lista)
            agregar_data(linea, self.archivo)

            # Se vuelve a actualizar la informacion mostrada
            self.cargar_del_csv(self.archivo)
        dialogo.destroy()


    def on_btn_delete(self, btn):
        """ Se elimina una entrada del archivo """
        # Se verifica la decision del usuario
        principal = f"Eliminar entrada"
        secundaria = f"¿Seguro que quiere eliminar una entrada?"
        if not on_question_clicked(self, principal, secundaria):
            return

        model, it = self.tree.get_selection().get_selected()
        if model is None or it is None:
            on_error_clicked(self,"Error", "No se ha seleccionado fila")
            return

        index = self.tree.get_selection().get_selected_rows()[1][0][0]
        # Se elimina la fila del dataframe
        data = pd.read_csv(self.archivo)
        data.drop([index], inplace=True, axis=0)
        # Se guarda de dataframe a archivo
        data.to_csv(self.archivo, index=False)

        # Se actualiza la informacion mostrada en el treeview
        self.cargar_del_csv(self.archivo)
        self.lbl_review.set_label("")


    def on_btn_about(self, btn):
        """ Se muestra la ventana About """
        AboutDialog()


if __name__ == "__main__":
    window = MainWindow()
    Gtk.main()
