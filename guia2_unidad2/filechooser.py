# Filechooser

# Se importan los modulos necesarios
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk

class FileChooser(Gtk.FileChooserDialog):
    """ FileChooserDialog en modo seleccionar archivo """
    def __init__(self):
        super().__init__(title="Seleccionar Archivo")
        self.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        self.set_action(Gtk.FileChooserAction.OPEN)

        # Filtro para aceptar todo archivo
        filter = Gtk.FileFilter()
        filter.set_name("Archivos")
        filter.add_pattern("*")
        self.add_filter(filter)
