# messages para verficar decisiones

from gi.repository import Gtk

def on_error_clicked(widget, principal, secundaria):
    """ Dialogo de error asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.ERROR,
        buttons=Gtk.ButtonsType.CANCEL,
        text=principal
    )
    dialog.format_secondary_text(
        secundaria
    )
    dialog.run()
    dialog.destroy()


def on_info_clicked(widget, principal, secundaria):
    """ Dialogo de info asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.INFO,
        buttons=Gtk.ButtonsType.OK,
        text=principal
    )
    dialog.format_secondary_text(
        secundaria
    )
    dialog.run()
    dialog.destroy()


def on_question_clicked(widget, principal, secundaria):
    """ Dialogo de pregunta asociado a un widget, muestra 2 mensajes """
    dialog = Gtk.MessageDialog(
        transient_for=widget,
        flags=0,
        message_type=Gtk.MessageType.QUESTION,
        buttons=Gtk.ButtonsType.YES_NO,
        text=principal
    )
    dialog.format_secondary_text(
        secundaria
    )
    response = dialog.run()
    if response == Gtk.ResponseType.YES:
        resp = True
    elif response == Gtk.ResponseType.NO:
        resp = False
    dialog.destroy()
    return resp
