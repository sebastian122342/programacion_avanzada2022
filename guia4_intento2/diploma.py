from asignatura import Asignatura

class Diploma():
    """ Diploma. Tiene un dueño y esta asociado a una asignatura """

    def __init__(self, estudiante, asignatura):
        # Constructor
        self._estudiante = estudiante
        self._asignatura = asignatura

    # Getter y setter del objeto estudiante
    @property
    def estudiante(self):
        return self._estudiante

    @estudiante.setter
    def estudiante(self, nombre):
        if isinstance(nombre, Estudiante):
            self._estudiante = nombre
        else:
            print("no pertenece a clase adecuada")

    # Getter y setter del objeto asignatura
    @property
    def asignatura(self):
        return self._asignatura

    @asignatura.setter
    def asignatura(self,asignatura):
        if isinstance(asignatura, Asignatura):
            self._asignatura = asignatura
        else:
            print("no pertenece a clase adecuada")
