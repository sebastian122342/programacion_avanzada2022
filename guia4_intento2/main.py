"""
Guia 4 programacion avanzada - intento 2
Sebastian Bustamante y Denise Valdes
"""

import random
from estudiante import Estudiante
from asignatura import Asignatura
from diploma import Diploma

def mostrar_info(lista):
    for i in lista:
        print(f"Estudiante {i.nombre} está en:")
        for asig in i.asignatura:
            print(asig.nombre)
        print("\nTiene los diplomas:")
        for d in i.diploma:
            print(f" de {d.asignatura.nombre} que le pertenece a {d.estudiante.nombre}")
        print("\n"+"*"*50+"\n")


def main():
    #objetos diplomas, estudiantes y cursos
    nombres = ["Pedro", "Josefa", "Manuela", "Sebastian", "Carla", "Emilios",
                "Jose", "Alejandra", "Catalina", "Pepe", "Carlos", "Martina"]
    asignaturas = [Asignatura("Mineria de datos"), Asignatura("Fisica"),
                        Asignatura("Bioinformatica estructural"),
                        Asignatura("Programacion Avanzada")]
    diplomas = []

    # deberia haber un minimo de 4 alumnos y un maximo de 12
    # son 4 asignaturas, recibiendo 3 diplomas cada 1
    # o recibiendo 1 cada 1, teniendo 12 alumnos max

    # Se crean estudiantes con nombres aleatorios
    # se permite que los nombres se repitan pues igual seran objetos distintos
    lista = []
    cant_alumnos = random.randint(len(asignaturas), len(asignaturas)*3)
    for x in range(cant_alumnos):
        lista.append(Estudiante(random.choice(nombres)))

    # repartir las asignaturas de manera correcta
    # se crea una lista con cant_asignaturas*3 objetos asignatura, se repiten.
    usables = []
    for a in asignaturas:
        for x in range(0, 3):
            usables.append(a)

    # Se agrega una asignatura a cada alumno, cada quien debe estar como minimo
    # en 1 asignatura. Se elimina la asignatura escogida de la lista de usables
    for i in lista:
        asig = random.choice(usables)
        i.asignatura = asig
        usables.remove(asig)

    # Se termina de agregar las asignaturas hasta que la lista de usables quede
    # vacia. No se repiten asignaturas
    while len(usables) > 0:
        est = random.choice(lista)
        asig = random.choice(usables)
        if not asig in est.asignatura:
            est.asignatura = asig
            usables.remove(asig)

    # Se crean todos los diplomas, se guardan en una lista
    diplomas = []
    for i in lista:
        for j in i.asignatura:
            diplomas.append(Diploma(i,j))

    # Se reparten los diplomas de forma aleatoria a los alumnos
    for i in range(len(diplomas)):
        alumno_escogido = random.choice(lista)
        diploma_repartido = random.choice(diplomas)
        alumno_escogido.diploma = diploma_repartido
        diplomas.remove(diploma_repartido)

    # Se muestra como queda la situacion justo despues de reordenar diplomas
    print("ASI ESTAN DESORDENADOS")
    mostrar_info(lista)

    # Los alumnos reordenan sus diplomas
    list_aux = []
    for i in lista:
        for d in i.diploma:
            if not d.estudiante is i:
                list_aux.append(d)
        for d in list_aux:
            d.estudiante.diploma = d
            i.diploma.remove(d)
        list_aux.clear()

    # Se muestra la situacion luego del orden. cada quien tiene sus diplomas
    print("\n\nASI ESTAN ORDENADOS")
    mostrar_info(lista)

if __name__ == '__main__':
    main()
