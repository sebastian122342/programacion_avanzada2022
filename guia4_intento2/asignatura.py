class Asignatura():
    """Asignatura. Tiene un nombre que es una string"""

    def __init__(self, nombre):
        # Constructor
        self._nombre = nombre

    # Getter del nombre de la asignatura
    @property
    def nombre(self):
        return self._nombre
