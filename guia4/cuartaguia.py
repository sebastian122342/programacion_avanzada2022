import random as rd

class Diploma():
    """ clase asociada al diploma. tiene dueño, ranking y asignatura """
    def __init__(self, asignatura, duenio, ranking):
        self._asignatura = asignatura
        self._duenio = duenio
        self._ranking = ranking

    @property
    def asignatura(self):
        return self._asignatura

    @property
    def duenio(self):
        return self._duenio

    @property
    def ranking(self):
        return self._ranking

    @asignatura.setter
    def asignatura(self, str):
        self._asignatura = str

    @duenio.setter
    def duenio(self, str):
        self._duenio = str

    @ranking.setter
    def ranking(self, num):
        self._ranking = num


class Estudiante():
    """ Clase del estudiante. tiene un nombre y una lista de diplomas """
    def __init__(self, nombre):
        self._nombre = nombre
        self._diplomas = []


    @property
    def nombre(self):
        return self._nombre

    @property
    def diploma(self):
        return self._diplomas

    @diploma.setter
    def diploma(self, diploma):
        self._diplomas.append(diploma)


def reparto(lista_estudiantes):
    """ Se reparten los diplomas de forma correcta los estudiantes. """
    for estudiante in lista_estudiantes:
        for diploma in estudiante.diploma:
            if diploma.duenio != estudiante.nombre:
                estudiante.diploma.remove(diploma)
                for estudiante2 in lista_estudiantes:
                    if diploma.duenio == estudiante2.nombre:
                        receptor = estudiante2
                        receptor.diploma = diploma
                        break
    return lista_estudiantes



if __name__ == "__main__":
    cant_estudiantes = rd.randint(4, 12)

    nombres = ["jose", "manuel", "pedro", "arturo", "maria", "jozuela",
                "manolita", "laura", "diego", "maximo", "robert", "margarita"]
    asignaturas = ["Fisica", "programacion", "mineria de datos", "bioinfo est."]
    nombres_usables = rd.sample(nombres, cant_estudiantes)

    diploma = []

    # se crean diplomas, todavia no es poseido por ningun estudiante
    nombres_no_usables = []
    for ramo in asignaturas:
        for x in range(0, 3):
            nombre_esp = rd.choice(nombres_usables)
            while nombre_esp in nombres_no_usables:
                nombre_esp = rd.choice(nombres_usables)
            diploma.append(Diploma(ramo, nombre_esp, x+1))
            nombres_no_usables.append(nombre_esp)
        nombres_no_usables = []

    # crear estudiantes

    lista_estudiantes = []

    for x in range(0, cant_estudiantes-1):
        nombre_esp = rd.choice(nombres_usables)
        lista_estudiantes.append(Estudiante(nombre_esp))
        nombres_usables.remove(nombre_esp)

    # se reparten los diplomas
    while len(diploma) > 0:
        estudiante_premiado = rd.choice(lista_estudiantes)
        diploma_repartido = rd.choice(diploma)
        estudiante_premiado.diploma = diploma_repartido
        diploma.remove(diploma_repartido)

    print("MAL ORDENADOS")
    for estudiante in lista_estudiantes:
        print(f"ESTUDIANTE: {estudiante.nombre}")
        print("TIENE LOS DIPLOMAS")
        for diploma in estudiante.diploma:
            print(f"{diploma.duenio}, {diploma.asignatura}, {diploma.ranking}\n")


    # Se crea una nueva lista ordenada, como queda con un desfase de llama dos veces
    print("BIEN ORDENADOS")
    lista_nueva = reparto(lista_estudiantes)
    lista_nueva = reparto(lista_nueva)
    lista_nueva = reparto(lista_nueva)
    for estudiante in lista_nueva:
        print(f"ESTUDIANTE: {estudiante.nombre}")
        print("TIENE LOS DIPLOMAS")
        for diploma in estudiante.diploma:
            print(f"{diploma.duenio}, {diploma.asignatura}, {diploma.ranking}\n")
