# Ejercicio 2

class Vacuna():
    def __init__(self, nombre, lab):
        # Constructor de la clase, con nombre de la vacuna y su lab
        self.nombre = nombre
        self.lab = lab
        self.efectos_secundarios = []

    def set_agrega_efecto_secundario(self, efecto):
        # Se agrega un efecto secundario a la lista.
        # en caso de que ya haya sido agregado, no se agrega de nuevo.
        if efecto not in self.efectos_secundarios:
            self.efectos_secundarios.append(efecto)
        else:
            print("El efecto ya ha sido agregado")

    def mostrar_efectos_secundarios(self):
        # Se muestran los efectos secundarios que tenga una vacuna.
        # tambien hay un mensaje en caso de que no hayan efectos.
        print(f"VACUNA {self.nombre} de {self.lab}")
        if self.efectos_secundarios == []:
            print("Aun no se agregan efectos secundarios\n")
        else:
            for efecto in self.efectos_secundarios:
                print(f"{efecto}")
            print("\n")

if __name__ == "__main__":
    # vacuna 1
    vacuna1 = Vacuna("Pfizer", "lab4324")
    vacuna1.set_agrega_efecto_secundario("dolor de cabeza")
    vacuna1.set_agrega_efecto_secundario("mareo")
    vacuna1.mostrar_efectos_secundarios()

    # vacuna 2
    vacuna2 = Vacuna("maria", "instituto de biologia")
    vacuna2.set_agrega_efecto_secundario("Vomito")
    vacuna2.mostrar_efectos_secundarios()

    # vacuna 3
    vacuna3 = Vacuna("influenza", "bayern")
    vacuna2.set_agrega_efecto_secundario("Paralisis facial")
    vacuna2.mostrar_efectos_secundarios()
