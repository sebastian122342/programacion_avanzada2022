#Ejercicio 1
from perro import mascota
#importación de la clase

""" Ejercicio
Crear una clase para un \Perro" necesita ser sacado a pasear. Este perro solo saldr ́a si
tom ́o un agua hace m ́as de 4 horas. En un archivo perro.py y como parte de la clase ya mencionada
se tienen tres metodos: set tomar agua(int), que permite especificar el tiempo (horas) con un n ́umero
entero; get hora toma agua(), que devuelve la hora de en que bebi ́o agua el perro; y caminar(), que si
es verdadero (True) indicar ́a si el perro necesita salir a pasear, caso contrario ser ́a falso (False)
"""
def menu(perro):

    # itenerador para las horas
    i = 0
    # itenerador de cantidad de horas sin tomar agua
    i2 = 0
    # bandera para el ciclo
    flag = True
    while flag:
        print("\nVan:",i, " horas")
        print("\nVan:",perro.get_hora(), " horas sin beber agua")
        print("Presiona P para pasear al perro: ")
        print("Presiona T para parar el paseo al perro: ")
        print("Presiona M para abandonar el proceso:")
        print("Presiona A para dar agua: ")
        opc = input("\t")
        # se define con el menu lo que escoge el usuario
        if opc.upper() == 'P' and perro.get_hora()>4:
            perro.set_paseo(True)
            print("se pasea")
            perro.set_tomar_agua(perro.get_hora()+1)

        elif opc.upper() == 'P' and perro.get_hora()<=4:
            print("No se pasea, porque falta tiempo sin aguas")
            perro.set_tomar_agua(perro.get_hora()+1)

        elif opc.upper() == 'T' and perro.get_paseo() == True:
            perro.set_paseo(False)
            print("se dejo de pasear")
            perro.set_tomar_agua(perro.get_hora()+1)

        elif opc.upper() == 'T' and perro.get_paseo() == False:
            print("no se puede dejar de pasear porque esta sin pasearse")
            perro.set_tomar_agua(perro.get_hora()+1)


        # para el caso en que deje de pasear el perro y aun este paseandolo
        elif opc.upper() == 'A':
            if perro.get_paseo() == False:
                perro.set_tomar_agua(0)
                print("le dio de beber al perro")
            else:
                print("no se puede porque esta paseandolo")
                perro.set_tomar_agua(perro.get_hora()+1)
        elif opc.upper() == 'M':
            flag = False
        else:
            perro.set_tomar_agua(perro.get_hora()+1)
            print("paso el tiempo")
        i = i+1


def main():
    perro = mascota()
    menu(perro)

if __name__ == '__main__':
    main()
