#Ejercicio 1
from perro import mascota
#importación de la clase

""" Ejercicio
Crear una clase para un \Perro" necesita ser sacado a pasear. Este perro solo saldr ́a si
tom ́o un agua hace m ́as de 4 horas. En un archivo perro.py y como parte de la clase ya mencionada
se tienen tres m ́etodos: set tomar agua(int), que permite especificar el tiempo (horas) con un n ́umero
entero; get hora toma agua(), que devuelve la hora de en que bebi ́o agua el perro; y caminar(), que si
es verdadero (True) indicar ́a si el perro necesita salir a pasear, caso contrario ser ́a falso (False)
"""
def menu(perro):

    i = 0
    i2 = 0
    flag = True
    while flag:
        print("\nVan:",i, " horas")
        print("\nVan:",perro.get_hora(), " horas de beber")
        opc = str(input("Presiona p para pasear al perro: "))

        if opc.upper() == 'P' and perro.get_hora()>4:
            perro.set_paseo(True)
            print("se pasea")
        else:
            print("No se paseo")
        opc = str(input("Presiona T para parar el paseo al perro: "))

        if opc.upper() == 'T' and perro.set_paseo == False:
            perro.set_paseo(False)
            print("se dejo de pasear")
        else:
            print("no se termino el paseo")
        opc = str(input("Presiona a para dar agua: "))

        if opc.upper() == 'A':

            if perro.get_paseo() == False:
                perro.set_tomar_agua(0)
                print("le dio de beber al perro")
        else:
            i2 = i2+1
            perro.set_tomar_agua(i2)
            print("no se puede")

        opc = str(input("Presiona s para abandonar al perro: "))
        if opc.upper() == 'S':
            flag = False
        i = i+1


def main():
    perro = mascota()
    menu(perro)

if __name__ == '__main__':
    main()
